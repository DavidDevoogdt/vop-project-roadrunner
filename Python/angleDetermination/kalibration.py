import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from datetime import datetime, timedelta
import math



for j in range(1,6,1):
    

    df = pd.read_csv("readings/A_%d.csv"%j, delimiter=";", header=0, index_col =0 )

    df[['ax','ay','az']] = df[['ax','ay','az']]
    endtime = df.iloc[-1]['seconds']
    #print(endtime)

    ################bepaling hoeken
    # angle rotation Rx(theta) -> Rz'(phi) -> Rz(omega)
    #theta  +-= 0.044
    #phi +-= 2.20
    #omega +-= 172*Pi/180
    ##########################

    tspan = 1

    azr = df[  df['seconds'] > endtime- tspan]['az']
    axr = df[  df['seconds'] > endtime- tspan]['ax']
    ayr = df[  df['seconds'] > endtime- tspan]['ay']
    gr = (azr**2+ axr**2+ayr**2)**0.5
    
    ##########first avaraging, then calc  (ac)
    
    azm = float(azr.mean())
    axm = float(axr.mean())
    aym = float(ayr.mean())
    gm =  float( gr.mean())

    mtheta = math.acos( azm /gm )
    mtheta1 = math.atan( math.sqrt(axm**2+ aym**2)/azm )   
    mphi = math.atan2(axm,-aym )
    
    #########"first calc, then averaging

    df['theta1'] = np.arccos( azr /gr)
    df['theta'] = np.arctan( (axr**2+ ayr**2)**0.5/azr) 
    df['phi'] = np.arctan2(axr,-ayr )

    ctheta = float(df['theta'].mean())
    ctheta1 = float(df['theta1'].mean())
    cphi = float(df['phi'].mean())
    

    #df['ax1'] = math.sin(phi)*df['ay'] + math.cos(phi)*df['ax']
    #df['ay1'] = -math.sin(phi)*math.cos(theta)*df['ax'] +math.cos(phi)*math.cos(theta)*df['ay']+math.sin(theta)*df['az']
    #df['az0'] = math.sin(phi)*math.sin(theta)*df['ax']-math.cos(phi)*math.sin(theta)*df['ay']+math.cos(theta)*df['az']

    ###########determinatin froward direction: acceleration perpendicular to car is nearly zero, searching for best fit in steps of 10 en 1

    #currentBest = None
    #bestAngle = None

    #for hoek in range(0,360,10):
        #omega= (hoek*3.1415)/180
        #df['ax0'] = math.cos(omega)*df['ax1']+ math.sin(omega)*df['ay1']
        #df['ay0'] = -math.sin(omega)*df['ax1']+ math.cos(omega)*df['ay1']
        #score = (df['ay0']**2) .sum()
        
        #if currentBest==None:
            #currentBest = score
            #bestAngle = hoek
        #if score < currentBest:
            #currentBest = score
            #bestAngle = hoek
            

    #for hoek in range(bestAngle-7,bestAngle+7,1):
        #omega= (hoek*3.1415)/180
        #df['ax0'] = math.cos(omega)*df['ax1']+ math.sin(omega)*df['ay1']
        #df['ay0'] = -math.sin(omega)*df['ax1']+ math.cos(omega)*df['ay1']
        #score = (df['ay0']**2) .sum()

        #if score < currentBest:
            #currentBest = score
            #bestAngle = hoek 
    
    ##print("beste hoek %f, kan 180° mis staan"%bestAngle)
    ##if( input("flip [Y/n]? ")=="Y"):
    ##    bestAngle =(bestAngle+180)%360
    
    #omega= ((bestAngle)*3.1415)/180
    #df['ax0'] = math.cos(omega)*df['ax1']+ math.sin(omega)*df['ay1']
    #df['ay0'] = -math.sin(omega)*df['ax1']+ math.cos(omega)*df['ay1']

    
    omega = 0
    
    
    print("g:%f,mtheta:%f, mtheta1:%f,ctheta:%f, ctheta1:%f,mphi:%f,cphi:%f,omega:%f"%(gm,mtheta,mtheta1,ctheta,ctheta1,mphi,cphi,omega))
    
    
