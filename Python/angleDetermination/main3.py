 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from datetime import datetime, timedelta
import math


name = "readings/A_5"

df = pd.read_csv(name+'.csv', delimiter=";", header=0, index_col =0 )

df[['ax','ay','az']] = df[['ax','ay','az']]*9.81
endtime = df.iloc[-1]['seconds']
#print(endtime)


plt.figure(39)
plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(40)
plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
plt.legend()
plt.draw()
plt.pause(0.001)

omega= 6.0386


print(omega)

df['ax0'] = math.cos(omega)*df['ax']+ math.sin(omega)*df['ay']
df['ay0'] = -math.sin(omega)*df['ax']+ math.cos(omega)*df['ay']
df['az0'] = df['az']


plt.figure(2)
plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(4)
plt.plot( df['seconds'], df['az0'], label="az geroteerd")
plt.plot( df['seconds'], df['ax0'], label="ax geroteerd")
plt.plot( df['seconds'], df['ay0'], label="ay geroteerd")
plt.legend()
plt.draw()
plt.pause(0.001)

 
omega= -0.01745

 
df['ay1'] = math.cos(omega)*df['ay0']+ math.sin(omega)*df['az0']
df['az1'] = -math.sin(omega)*df['ay0']+ math.cos(omega)*df['az0']
df['ax1'] = df['ax0']

plt.figure(5)
plt.plot( df['seconds'], df['az1'], label="az geroteerd")
plt.plot( df['seconds'], df['ax1'], label="ax geroteerd")
plt.plot( df['seconds'], df['ay1'], label="ay geroteerd")
plt.legend()
plt.draw()
plt.pause(0.001)


hoekmin = 1
hoekmax = 2.5
steps = 5

#for i in range(0,steps,1):
#    hoekCorr = hoekmin + i /steps *(hoekmax-hoekmin)
if(True):
    hoekCorr = 1.7
    
    thetaCorr = hoekCorr*3.1415/180
    
    df['ax2'] =  math.cos(thetaCorr)*df['ax1'] + math.sin(thetaCorr)*df['az1']


    ############### setup kalman
    row = df.iloc[0]
    a0 = float(row['ax2'])
    v0 = float(row['gpsspeed']) 

    x = np.matrix([v0,a0]).T #initial state matrix # vx ax

    #P = np.diag([0.1, 0.001]) #initial uncertainity
    P = np.array([[1.11 ,9*10**(-5) ],
                    [9*10**(-5), 9*10**(-5) ]])

    dt = 0.01
    A = np.matrix([[1, dt],
                [0,1]]) #dynamics matrix

    ra = 0.10**2   #std
    rv = 0.5**2 
    ### with gps
    H1 = np.matrix([[ 1.0, 0.0],
                [ 0.0, 1.0]])   #measurement matrix

    R1 = np.matrix([[ra, 0.0],
                [0.0, ra]])    # measurement noise covariance

    ### without GPS
    H2 = np.matrix([[ 0.0, 1.0]])  #measurement matrix

    R2 = np.matrix([[ ra]])# measurement noise covariance

    ### Process Noise Covariance Matrix 
    sa = 0.001
    G = np.matrix([[dt],
                [1.0]])
    Q = G*G.T*sa**2

    I = np.eye(2)
    #################### kalman calc

    def predict(A,x,P):
        x = A*x  # Project the state ahead
        P = A*P*A.T + Q # Project the error covariance ahead
        return (x,P)
    def correct(H,P,R,M,x):
        K = (P*H.T) * np.linalg.pinv( H*P*H.T + R)  # Compute the Kalman Gain
        x = x + K*(M - (H*x)) # Update the estimate via M
        P = (I - (K*H))*P # Update the error covariance
        return x,P

    for i,row in df[1:].iterrows():
        row = df.loc[[i]]
        vx = float(row['gpsspeed'])
        
        a = float(row['ax2'])
        v = float(row['gpsspeed']) 
        
        
        gps = int(row['gps']) == 1
        print(x)
        
        x,P = predict(A,x,P)
        print(x)
        
        if gps:
            x,P = correct(H1,P,R1, np.array([[v],[a]]),x ) #accerleration and gps data
        else : 
            x,P = correct(H2,P,R2, [a] ,x )  #only accerleration data
            
        print(x)
        df.loc[[i],'v_kalman'] =  x[0]

        


    print( P)
    plt.figure(8)
    plt.plot( df['seconds'], df['v_kalman'],label="v kalman %f"%(thetaCorr*180/3.1415))
    plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
    plt.legend()
    plt.draw()
    plt.pause(0.001)
        
###

df['az2'] = -math.sin(thetaCorr)*df['ax1'] + math.cos(thetaCorr)*df['az1']
df['ay2'] = df['ay1']



#######plotjes
if input("save [Y/n]")=="Y":
    df[['seconds','az2','ax2','ay2','v_kalman']].to_csv( name +'_K'+'.csv', sep=';')



