import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from datetime import datetime, timedelta
import math




df = pd.read_csv('Converted/trip4_D_1.csv', delimiter=";", header=0, index_col =0 )

df[['ax','ay','az']] = df[['ax','ay','az']]*9.81
endtime = df.iloc[-1]['seconds']
#print(endtime)

plt.figure(1)
plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(2)
plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
plt.xlabel( "tijd [s]")
plt.ylabel("versnelling [m/s^2]")
plt.legend()
plt.draw()
plt.pause(0.001)



print("t1-t2 : a zo groot mogelijk, maar best niet stuk da correleerd met z, t3-t4: rustig mogelijk, versnelling uit gps zo klein mogelijk  ")
print("voor A_2: t1=1.6 t2=3  t3=9 t4 =10.5  ")

t1 =float(input("t1= "))
t2 = float(input("t2= "))
t3 = float(input("t3= "))
t4 = float(input("t4= "))



##a2
#t1 = 0
#t2 = 3
#t3=9
#t4=10.5
##c1
#t1 = 16
#t2 = 18
#t3=0
#t4=8


################bepaling hoeken
# angle rotation Rx(theta) -> Rz'(phi) -> Rz(omega)
#theta  +-= 0.044
#phi +-= 2.20
#omega +-= 172*Pi/180
##########################

tspan = 1

azr = df[ (t3< df['seconds'])*(df['seconds'] < t4)  ]['az']
axr = df[  (t3< df['seconds'])*(df['seconds'] < t4) ]['ax']
ayr = df[ (t3< df['seconds'])*(df['seconds'] < t4) ]['ay']
secR = df[ (t3< df['seconds'])*(df['seconds'] < t4) ]['seconds'] 
gr = (azr**2+ axr**2+ayr**2)**0.5
axc = df[ (t1< df['seconds'])*(df['seconds'] < t2) ]['ax'] 
ayc = df[ (t1< df['seconds'])*(df['seconds'] < t2) ]['ay'] 
azc = df[ (t1< df['seconds'])*(df['seconds'] < t2) ]['az'] 
secC = df[ (t1< df['seconds'])*(df['seconds'] < t2) ]['seconds'] 
print(axc)

##########first avaraging, then calc  (ac)

azm = float(azr.mean())
axm = float(axr.mean())
aym = float(ayr.mean())

#azc = float(azc.mean())
#axc = float(axc.mean())
#ayc = float(ayc.mean())

gm =  float( gr.mean())

#theta = math.acos( azm /gm )

if(input("theta known? [Y/n]") =="Y"):
    theta = float(eval(input("theta=")))
    thetaKnown = True
else:
    theta = math.atan( math.sqrt(axm**2+ aym**2)/azm )   #haal hier versnelling van gps uit
    thetaKnown = False
    
#omega = math.atan2(axm,aym)


currentBest = None
bestAngle = None
df['s1'] = axr
df['s2'] = ayr

for hoek in range(0,360,10):
    omega= (hoek*3.1415)/180
    df['s0_0'] = math.cos(omega)*df['s1']+ math.sin(omega)*df['s2']
    df['s0_1'] = -math.sin(omega)*df['s1']+ math.cos(omega)*df['s2']
    
    score = (df['s0_1']**2).sum()
    
    if currentBest==None:
        currentBest = score
        bestAngle = hoek
    if score < currentBest:
        currentBest = score
        bestAngle = hoek
        

for hoek in range(bestAngle-7,bestAngle+7,1):
    omega= (hoek*3.1415)/180
    df['s0_0'] = math.cos(omega)*df['s1']+ math.sin(omega)*df['s2']
    df['s0_1'] = -math.sin(omega)*df['s1']+ math.cos(omega)*df['s2']
    
    score = (df['s0_0']**2).sum()
    
    if currentBest==None:
        currentBest = score
        bestAngle = hoek
    if score < currentBest:
        currentBest = score
        bestAngle = hoek
 
omega= ((bestAngle)*3.1415)/180
if input("flip [Y/n]")=="Y":
    omega = omega - 3.1415192

print(omega)




#omegaplusphi = math.atan2(ayc,-axc)


#plt.figure(61)
#plt.plot( secC, np.arctan2(ayc,-axc), label="omega + phi")
#plt.plot( secR, np.arctan( np.sqrt(axr**2+ ayr**2)/azr ), label="theta")
#plt.legend()
#plt.draw()
#plt.pause(0.001)



omegaplusphi = float( np.arctan2(ayc,-axc).mean())

phi = omegaplusphi - omega

print("%f %f %f"%(theta,omega,phi))

################## inverse transfo

df['ax0'] = (math.cos(phi)*math.cos(omega) - math.sin(phi)*math.sin(omega)*math.cos(theta))*df['ax']+ ( math.sin(phi)*math.cos(omega) + math.cos(phi)*math.cos(theta)*math.sin(omega))*df['ay']+ math.sin(omega)*math.sin(theta)*df['az']
df['ay0'] = (-math.cos(phi)*math.sin(omega) - math.sin(phi)*math.cos(omega)*math.cos(theta))*df['ax']+ ( -math.sin(phi)*math.sin(omega) + math.cos(phi)*math.cos(theta)*math.cos(omega))*df['ay']+ math.cos(omega)*math.sin(theta)*df['az']
df['az0'] = -math.cos(phi)*math.sin(theta) *df['ay'] + math.sin(phi)*math.sin(theta)*df['ax']  + math.cos(theta)*df['az']

#######plotjes


plt.figure(4)
plt.plot( df['seconds'], df['az0'], label="az geroteerd")
plt.plot( df['seconds'], df['ax0'], label="ax geroteerd")
plt.plot( df['seconds'], df['ay0'], label="ay geroteerd")
plt.xlabel( "tijd [s]")
plt.ylabel("versnelling [m/s^2]")
plt.legend()
plt.draw()
plt.pause(0.001)



 #input()

################## bijna juist , az fixen op kalman


if(thetaKnown):
    xmin = -0.5
    xmax = 0.5
    steps = 5
else:
    xmin = 0
    xmax = 5
    steps = 10



for i in range(0,steps,1):
    hoekCorr = xmin + i /steps *( xmax -xmin)
#if(True):
#    hoekCorr = 0
    
    
    thetaCorr = hoekCorr*3.1415/180
    
    df['axCorr'] =  math.cos(thetaCorr)*df['ax0'] + math.sin(thetaCorr)*df['az0']
    df['azCorr'] = -math.sin(thetaCorr)*df['ax0'] + math.cos(thetaCorr)*df['az0']
    


    ############### setup kalman
    row = df.iloc[0]
    a0 = float(row['axCorr'])
    v0 = float(row['gpsspeed']) 

    x = np.matrix([v0,a0]).T #initial state matrix # vx ax

    #P = np.diag([0.1, 0.001]) #initial uncertainity
    P = np.array([[1.11 ,9*10**(-5) ],
                    [9*10**(-5), 9*10**(-5) ]])

    dt = 0.01
    A = np.matrix([[1, dt],
                [0,1]]) #dynamics matrix

    ra = 0.10**2   #std
    rv = 0.5**2 
    ### with gps
    H1 = np.matrix([[ 1.0, 0.0],
                [ 0.0, 1.0]])   #measurement matrix

    R1 = np.matrix([[ra, 0.0],
                [0.0, ra]])    # measurement noise covariance

    ### without GPS
    H2 = np.matrix([[ 0.0, 1.0]])  #measurement matrix

    R2 = np.matrix([[ ra]])# measurement noise covariance

    ### Process Noise Covariance Matrix 
    sa = 0.001
    G = np.matrix([[dt],
                [1.0]])
    Q = G*G.T*sa**2

    I = np.eye(2)
    #################### kalman calc

    def predict(A,x,P):
        x = A*x  # Project the state ahead
        P = A*P*A.T + Q # Project the error covariance ahead
        return (x,P)
    def correct(H,P,R,M,x):
        K = (P*H.T) * np.linalg.pinv( H*P*H.T + R)  # Compute the Kalman Gain
        x = x + K*(M - (H*x)) # Update the estimate via M
        P = (I - (K*H))*P # Update the error covariance
        return x,P

    for i,row in df[1:].iterrows():
        row = df.loc[[i]]
        vx = float(row['gpsspeed'])
        
        a = float(row['axCorr'])
        v = float(row['gpsspeed']) 
        
        
        gps = int(row['gps']) == 1
        print(x)
        
        x,P = predict(A,x,P)
        print(x)
        
        if gps:
            x,P = correct(H1,P,R1, np.array([[v],[a]]),x ) #accerleration and gps data
        else : 
            x,P = correct(H2,P,R2, [a] ,x )  #only accerleration data
            
        print(x)
        df.loc[[i],'v_kalman'] =  x[0]

        


    print( P)
    plt.figure(8)
    plt.plot( df['seconds'], df['v_kalman'],label="v kalman")
    plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
    plt.xlabel( "tijd [s]")
    plt.ylabel("snelheid [m/s]")
    plt.legend()
    plt.draw()
    plt.pause(0.001)
        
###

dtheta = float(input("best one?"))
print("theta %f, theta +dtheta %f, phi %f omega %f"%(theta,theta + dtheta*3.1415/180,phi,omega))


input()
