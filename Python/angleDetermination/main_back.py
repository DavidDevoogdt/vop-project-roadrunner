import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from datetime import datetime, timedelta
import math




df = pd.read_csv('readings/C_1.csv', delimiter=";", header=0, index_col =0 )

df[['ax','ay','az']] = df[['ax','ay','az']]*9.81
endtime = df.iloc[-1]['seconds']
#print(endtime)

plt.figure(1)
plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(2)
plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
plt.legend()
plt.draw()
plt.pause(0.001)



print("t1-t2 : a zo groot mogelijk, maar best niet stuk da correleerd met z, t3-t4: rustig mogelijk  ")


t1 =float(input("t1"))
t2 = float(input("t1"))
t3 = float(input("t1"))
t4 = float(input("t1"))



################bepaling hoeken
# angle rotation Rx(theta) -> Rz'(phi) -> Rz(omega)
#theta  +-= 0.044
#phi +-= 2.20
#omega +-= 172*Pi/180
##########################

tspan = 1

azr = df[ (t3< df['seconds'])*(df['seconds'] < t4)  ]['az']
axr = df[  (t3< df['seconds'])*(df['seconds'] < t4) ]['ax']
ayr = df[ (t3< df['seconds'])*(df['seconds'] < t4) ]['ay']
gr = (azr**2+ axr**2+ayr**2)**0.5

##########first avaraging, then calc  (ac)

azm = float(azr.mean())
axm = float(axr.mean())
aym = float(ayr.mean())
gm =  float( gr.mean())

#theta = math.acos( azm /gm )

theta = math.atan( math.sqrt(axm**2+ aym**2)/azm )   

phi = math.atan2(axm,-aym )

##################


df['ax1'] = math.sin(phi)*df['ay'] + math.cos(phi)*df['ax']
df['ay1'] = -math.sin(phi)*math.cos(theta)*df['ax'] +math.cos(phi)*math.cos(theta)*df['ay']+math.sin(theta)*df['az']
df['az0'] = math.sin(phi)*math.sin(theta)*df['ax']-math.cos(phi)*math.sin(theta)*df['ay']+math.cos(theta)*df['az']

df['ax1r'] = df[  df['seconds']<t2 ]['ax1'] 
df['ay1r'] = df[  df['seconds']<t2 ]['ay1']


##########determinatin froward direction: acceleration perpendicular to car is nearly zero, searching for best fit in steps of 10 en 1

currentBest = None
bestAngle = None

for hoek in range(0,360,10):
    omega= (hoek*3.1415)/180
    df['ax0r'] = math.cos(omega)*df['ax1r']+ math.sin(omega)*df['ay1r']
    df['ay0r'] = -math.sin(omega)*df['ax1r']+ math.cos(omega)*df['ay1r']
    score = (df['ay0r']**2) .sum()
    
    if currentBest==None:
        currentBest = score
        bestAngle = hoek
    if score < currentBest:
        currentBest = score
        bestAngle = hoek
        

for hoek in range(bestAngle-7,bestAngle+7,1):
    omega= (hoek*3.1415)/180
    df['ax0r'] = math.cos(omega)*df['ax1r']+ math.sin(omega)*df['ay1r']
    df['ay0r'] = -math.sin(omega)*df['ax1r']+ math.cos(omega)*df['ay1r']
    score = (df['ay0r']**2) .sum()

    if score < currentBest:
        currentBest = score
        bestAngle = hoek 
 
print("beste hoek %f, kan 180° mis staan"%bestAngle)
if( input("flip [Y/n]? ")=="Y"):
    bestAngle =(bestAngle+180)%360
 
omega= ((bestAngle)*3.1415)/180
df['ax0'] = math.cos(omega)*df['ax1']+ math.sin(omega)*df['ay1']
df['ay0'] = -math.sin(omega)*df['ax1']+ math.cos(omega)*df['ay1']




#######plotjes


plt.figure(2)
plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(4)
plt.plot( df['seconds'], df['az0'], label="az geroteerd")
plt.plot( df['seconds'], df['ax0'], label="ax geroteerd")
plt.plot( df['seconds'], df['ay0'], label="ay geroteerd")
plt.legend()
plt.draw()
plt.pause(0.001)

#input()

################## bijna juist , az fixen op kalman


xmin = 0
xmax = 1
steps = 10

for i in range(0,steps,1):
    hoekCorr = xmin + i /steps *( xmax -xmin)
#if(True):
#    hoekCorr = 0
    
    
    thetaCorr = hoekCorr*3.1415/180
    
    df['axCorr'] =  math.cos(thetaCorr)*df['ax0'] + math.sin(thetaCorr)*df['az0']
    df['azCorr'] = -math.sin(thetaCorr)*df['ax0'] + math.cos(thetaCorr)*df['az0']
    


    ############### setup kalman
    row = df.iloc[0]
    a0 = float(row['axCorr'])
    v0 = float(row['gpsspeed']) 

    x = np.matrix([v0,a0]).T #initial state matrix # vx ax

    #P = np.diag([0.1, 0.001]) #initial uncertainity
    P = np.array([[1.11 ,9*10**(-5) ],
                    [9*10**(-5), 9*10**(-5) ]])

    dt = 0.01
    A = np.matrix([[1, dt],
                [0,1]]) #dynamics matrix

    ra = 0.10**2   #std
    rv = 0.5**2 
    ### with gps
    H1 = np.matrix([[ 1.0, 0.0],
                [ 0.0, 1.0]])   #measurement matrix

    R1 = np.matrix([[ra, 0.0],
                [0.0, ra]])    # measurement noise covariance

    ### without GPS
    H2 = np.matrix([[ 0.0, 1.0]])  #measurement matrix

    R2 = np.matrix([[ ra]])# measurement noise covariance

    ### Process Noise Covariance Matrix 
    sa = 0.001
    G = np.matrix([[dt],
                [1.0]])
    Q = G*G.T*sa**2

    I = np.eye(2)
    #################### kalman calc

    def predict(A,x,P):
        x = A*x  # Project the state ahead
        P = A*P*A.T + Q # Project the error covariance ahead
        return (x,P)
    def correct(H,P,R,M,x):
        K = (P*H.T) * np.linalg.pinv( H*P*H.T + R)  # Compute the Kalman Gain
        x = x + K*(M - (H*x)) # Update the estimate via M
        P = (I - (K*H))*P # Update the error covariance
        return x,P

    for i,row in df[1:].iterrows():
        row = df.loc[[i]]
        vx = float(row['gpsspeed'])
        
        a = float(row['axCorr'])
        v = float(row['gpsspeed']) 
        
        
        gps = int(row['gps']) == 1
        print(x)
        
        x,P = predict(A,x,P)
        print(x)
        
        if gps:
            x,P = correct(H1,P,R1, np.array([[v],[a]]),x ) #accerleration and gps data
        else : 
            x,P = correct(H2,P,R2, [a] ,x )  #only accerleration data
            
        print(x)
        df.loc[[i],'v_kalman'] =  x[0]

        


    print( P)
    plt.figure(8)
    plt.plot( df['seconds'], df['v_kalman'],label="v kalman %f"%hoekCorr)
    plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
    plt.legend()
    plt.draw()
    plt.pause(0.001)
        
###

#dtheta = float(input("best one?"))
#theta = theta + dtheta*3.1415/180
print("theta %f phi %f omega %f"%(theta,phi,omega))

input()
