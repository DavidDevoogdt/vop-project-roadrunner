import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from datetime import datetime, timedelta
import math

from scipy.interpolate import interp1d



df = pd.read_csv('Converted/trip4_E_1.csv', delimiter=";", header=0, index_col =0 )
df[['ax','ay','az']] = df[['ax','ay','az']]*9.81


omega = 2.268
phi = -2.113
theta = 0.0334



df['ax0'] = (math.cos(phi)*math.cos(omega) - math.sin(phi)*math.sin(omega)*math.cos(theta))*df['ax']+ ( math.sin(phi)*math.cos(omega) + math.cos(phi)*math.cos(theta)*math.sin(omega))*df['ay']+ math.sin(omega)*math.sin(theta)*df['az']
df['ay0'] = (-math.cos(phi)*math.sin(omega) - math.sin(phi)*math.cos(omega)*math.cos(theta))*df['ax']+ ( -math.sin(phi)*math.sin(omega) + math.cos(phi)*math.cos(theta)*math.cos(omega))*df['ay']+ math.cos(omega)*math.sin(theta)*df['az']
df['az0'] = -math.cos(phi)*math.sin(theta) *df['ay'] + math.sin(phi)*math.sin(theta)*df['ax']  + math.cos(theta)*df['az']

#plt.figure(1)
#plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
#plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
#plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
#plt.legend()
#plt.draw()
#plt.pause(0.001)
#
#
#
#plt.figure(2)
#plt.plot( df['seconds'], df['az0'], label="az geroteerd")
#plt.plot( df['seconds'], df['ax0'], label="ax geroteerd")
#plt.plot( df['seconds'], df['ay0'], label="ay geroteerd")
#plt.legend()
#plt.draw()
#plt.pause(0.001)



############### setup kalman
row = df.iloc[0]
a0 = float(row['ax0'])
v0 = float(row['gpsspeed']) 

x = np.matrix([v0,a0]).T #initial state matrix # vx ax

#P = np.diag([0.1, 0.001]) #initial uncertainity
P = np.array([[1.11 ,9*10**(-5) ],
                [9*10**(-5), 9*10**(-5) ]])

dt = 0.01
A = np.matrix([[1, dt],
            [0,1]]) #dynamics matrix

ra = 0.10**2   #std
rv = 0.5**2 
### with gps
H1 = np.matrix([[ 1.0, 0.0],
            [ 0.0, 1.0]])   #measurement matrix

R1 = np.matrix([[ra, 0.0],
            [0.0, ra]])    # measurement noise covariance

### without GPS
H2 = np.matrix([[ 0.0, 1.0]])  #measurement matrix

R2 = np.matrix([[ ra]])# measurement noise covariance

### Process Noise Covariance Matrix 
sa = 0.001
G = np.matrix([[dt],
            [1.0]])
Q = G*G.T*sa**2

I = np.eye(2)
#################### kalman calc

def predict(A,x,P):
    x = A*x  # Project the state ahead
    P = A*P*A.T + Q # Project the error covariance ahead
    return (x,P)
def correct(H,P,R,M,x):
    K = (P*H.T) * np.linalg.pinv( H*P*H.T + R)  # Compute the Kalman Gain
    x = x + K*(M - (H*x)) # Update the estimate via M
    P = (I - (K*H))*P # Update the error covariance
    return x,P

for i,row in df[1:].iterrows():
    row = df.loc[[i]]
    vx = float(row['gpsspeed'])
    
    a = float(row['ax0'])
    v = float(row['gpsspeed']) 
    
    
    gps = int(row['gps']) == 1
    
    x,P = predict(A,x,P)
    
    if gps:
        x,P = correct(H1,P,R1, np.array([[v],[a]]),x ) #accerleration and gps data
    else : 
        x,P = correct(H2,P,R2, [a] ,x )  #only accerleration data
        

    df.loc[[i],'v_kalman'] =  x[0]

df[['seconds','az0','v_kalman']].to_csv("kalmandata.csv", sep=';')

f_az=  interp1d(df['seconds'], df['az0'], kind='cubic')
f_kx= interp1d(df['seconds'], df['v_kalman'], kind='cubic')
#plt.figure(8)
#plt.plot
#plt.figure(8)
#plt.plot( df['seconds'], df['v_kalman'],label="v kalman")
#plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
#plt.legend()
#plt.draw()
#plt.pause(0.001)
#
#plt.show()
