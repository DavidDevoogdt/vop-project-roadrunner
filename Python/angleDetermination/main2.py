import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from datetime import datetime, timedelta
import math




df = pd.read_csv('readings/C_1.csv', delimiter=";", header=0, index_col =0 )

df[['ax','ay','az']] = df[['ax','ay','az']]*9.81
endtime = df.iloc[-1]['seconds']
#print(endtime)


plt.figure(39)
plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(40)
plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
plt.legend()
plt.draw()
plt.pause(0.001)
print("t1-t2 : az zo groot mogelijk, maar best niet stuk da correleerd met z, t3-t4: rustig mogelijk  ")

t1 =float(input("t1"))
t2 = float(input("t1"))
t3 = float(input("t1"))
t4 = float(input("t1"))


currentBest = None
bestAngle = None

df['s1'] = df[ (t1< df['seconds'])&(df['seconds']>t2)]['ax']
df['s2'] = df[ (t1< df['seconds'])&(df['seconds']>t2)]['ay']

for hoek in range(0,360,10):
    omega= (hoek*3.1415)/180
    df['s0_0'] = math.cos(omega)*df['s1']+ math.sin(omega)*df['s2']
    df['s0_1'] = -math.sin(omega)*df['s1']+ math.cos(omega)*df['s2']
    
    score = float(df['s0_1'].std())
    
    if currentBest==None:
        currentBest = score
        bestAngle = hoek
    if score < currentBest:
        currentBest = score
        bestAngle = hoek
        

for hoek in range(bestAngle-7,bestAngle+7,1):
    omega= (hoek*3.1415)/180
    df['s0_0'] = math.cos(omega)*df['s1']+ math.sin(omega)*df['s2']
    df['s0_1'] = -math.sin(omega)*df['s1']+ math.cos(omega)*df['s2']
    
    score = float(df['s0_1'].std())
    
    if currentBest==None:
        currentBest = score
        bestAngle = hoek
    if score < currentBest:
        currentBest = score
        bestAngle = hoek
 
omega= ((bestAngle)*3.1415)/180
if input("flip [Y/n]")=="Y":
    omega = omega - 3.1415192

print(omega)

df['ax0'] = math.cos(omega)*df['ax']+ math.sin(omega)*df['ay']
df['ay0'] = -math.sin(omega)*df['ax']+ math.cos(omega)*df['ay']
df['az0'] = df['az']


plt.figure(2)
plt.plot( df['seconds'], df['az'], label="az oorspronkelijk")
plt.plot( df['seconds'], df['ax'], label="ax oorspronkelijk")
plt.plot( df['seconds'], df['ay'], label="ay oorspronkelijk")
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(4)
plt.plot( df['seconds'], df['az0'], label="az geroteerd")
plt.plot( df['seconds'], df['ax0'], label="ax geroteerd")
plt.plot( df['seconds'], df['ay0'], label="ay geroteerd")
plt.legend()
plt.draw()
plt.pause(0.001)



#bepaal hoek v

df['s1'] = df[ (t3< df['seconds'])&(df['seconds']>t4)]['ay0']
df['s2'] = df[ (t3< df['seconds'])&(df['seconds']>t4)]['az0']


currentBest = None
bestAngle = None

for hoek in range(0,360,10):
    df['s0_0'] = math.cos(omega)*df['s1']+ math.sin(omega)*df['s2']
    df['s0_1'] = -math.sin(omega)*df['s1']+ math.cos(omega)*df['s2']
    
    score = (df['s0_0']**2).sum()
    
    if currentBest==None:
        currentBest = score
        bestAngle = hoek
    if score < currentBest:
        currentBest = score
        bestAngle = hoek
        

for hoek in range(bestAngle-7,bestAngle+7,1):
    omega= (hoek*3.1415)/180
    df['s0_0'] = math.cos(omega)*df['s1']+ math.sin(omega)*df['s2']
    df['s0_1'] = -math.sin(omega)*df['s1']+ math.cos(omega)*df['s2']
    
    score = (df['s0_0']**2).sum()
    
  
    
    
    if currentBest==None:
        currentBest = score
        bestAngle = hoek
    if score < currentBest:
        currentBest = score
        bestAngle = hoek
 
omega= ((bestAngle)*3.1415)/180

print(omega) 
 
df['ay1'] = math.cos(omega)*df['ay0']+ math.sin(omega)*df['az0']
df['az1'] = -math.sin(omega)*df['ay0']+ math.cos(omega)*df['az0']
df['ax1'] = df['ax0']

plt.figure(5)
plt.plot( df['seconds'], df['az1'], label="az geroteerd")
plt.plot( df['seconds'], df['ax1'], label="ax geroteerd")
plt.plot( df['seconds'], df['ay1'], label="ay geroteerd")
plt.legend()
plt.draw()
plt.pause(0.001)

input()


hoekmin =-2
hoekmax = 2
steps = 10

for i in range(0,steps,1):
    hoekCorr = hoekmin + i /steps *(hoekmax-hoekmin)
#if(True):
#    hoekCorr = 0
    
    thetaCorr = hoekCorr*3.1415/180
    
    df['axCorr'] =  math.cos(thetaCorr)*df['ax0'] + math.sin(thetaCorr)*df['az0']
    df['azCorr'] = -math.sin(thetaCorr)*df['ax0'] + math.cos(thetaCorr)*df['az0']
    


    ############### setup kalman
    row = df.iloc[0]
    a0 = float(row['axCorr'])
    v0 = float(row['gpsspeed']) 

    x = np.matrix([v0,a0]).T #initial state matrix # vx ax

    #P = np.diag([0.1, 0.001]) #initial uncertainity
    P = np.array([[1.11 ,9*10**(-5) ],
                    [9*10**(-5), 9*10**(-5) ]])

    dt = 0.01
    A = np.matrix([[1, dt],
                [0,1]]) #dynamics matrix

    ra = 0.10**2   #std
    rv = 0.5**2 
    ### with gps
    H1 = np.matrix([[ 1.0, 0.0],
                [ 0.0, 1.0]])   #measurement matrix

    R1 = np.matrix([[ra, 0.0],
                [0.0, ra]])    # measurement noise covariance

    ### without GPS
    H2 = np.matrix([[ 0.0, 1.0]])  #measurement matrix

    R2 = np.matrix([[ ra]])# measurement noise covariance

    ### Process Noise Covariance Matrix 
    sa = 0.001
    G = np.matrix([[dt],
                [1.0]])
    Q = G*G.T*sa**2

    I = np.eye(2)
    #################### kalman calc

    def predict(A,x,P):
        x = A*x  # Project the state ahead
        P = A*P*A.T + Q # Project the error covariance ahead
        return (x,P)
    def correct(H,P,R,M,x):
        K = (P*H.T) * np.linalg.pinv( H*P*H.T + R)  # Compute the Kalman Gain
        x = x + K*(M - (H*x)) # Update the estimate via M
        P = (I - (K*H))*P # Update the error covariance
        return x,P

    for i,row in df[1:].iterrows():
        row = df.loc[[i]]
        vx = float(row['gpsspeed'])
        
        a = float(row['axCorr'])
        v = float(row['gpsspeed']) 
        
        
        gps = int(row['gps']) == 1
        print(x)
        
        x,P = predict(A,x,P)
        print(x)
        
        if gps:
            x,P = correct(H1,P,R1, np.array([[v],[a]]),x ) #accerleration and gps data
        else : 
            x,P = correct(H2,P,R2, [a] ,x )  #only accerleration data
            
        print(x)
        df.loc[[i],'v_kalman'] =  x[0]

        


    print( P)
    plt.figure(8)
    plt.plot( df['seconds'], df['v_kalman'],label="v kalman %f"%thetaCorr)
    plt.plot( df[df['gps']==1]['seconds'], df[df['gps']==1]['gpsspeed'], label="gpsspeed")
    plt.legend()
    plt.draw()
    plt.pause(0.001)
        
###




#######plotjes




