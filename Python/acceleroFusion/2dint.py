import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import interpolate, signal

import matplotlib.pyplot as plt
import math
import time
from datetime import datetime


def runningAvg(series, N):
    padded = np.pad(series, (N // 2, N - 1 - N // 2), mode='edge')
    return np.convolve(padded, np.ones((N,)) / N, mode='valid')


filename1 = sys.argv[1]
filename2 = sys.argv[2]

df1 = pd.read_table(filename1, delimiter=";", header=0)
df2 = pd.read_csv(filename2, delimiter=",", header=0)

# ____________calibration_______________

# ax             0.121568506568460
# ay            -0.000054966195790
# az           -10.933853359534035
# gx            -2.356726986203252
# gy             0.810309998351138
# gz            -0.311074784257679


df1['time'] = df1['time'] - df1['time'][0]  # set t to zero
# df1['deltatime'] =  df1['time'] - df1.shift(periods=1)['time']

df2['time2'] = df2['time']
df2['az2'] = df2['az']
df2['ay2'] = df2['ay']
df2['ax2'] = df2['ax']


avgA2 = df2[df2['time2'] < 1].mean()
offsets = df1[df1['time'] < 1].mean()

c1 = 1 / 16384 * 9.8116
c2 = 1 / 131
# adjust for systematic bias (null point reference measurement
# df1['ax'] = -(df1['ax']*c1   - 0.121568506568460)
# df1['ay'] = -(df1['ay']*c1 + 0.000054966195790)
# df1['az'] = -(df1['az']*c1 + (10.933853359534035 - 9.8116))
# df1['gx'] = -(df1['gx']*c2+2.356726986203252)
# df1['gy'] = -(df1['gy']*c2 -0.810309998351138)
# df1['gz'] = -(df1['gz']*c2+0.311074784257679)

df1['ax'] = (df1['ax'] * c1 - 0.121568506568460)
df1['ay'] = (df1['ay'] * c1 + 0.000054966195790)
df1['az'] = -((df1['az'] * c1 + (10.933853359534035 - 9.8116)))
df1['gx'] = (df1['gx'] - offsets['gx']) * c2
df1['gy'] = (df1['gy'] - offsets['gy']) * c2
df1['gz'] = (df1['gz'] - offsets['gz']) * c2

df2['time2'] = df2['time2']/1.08836 #sampling rate acc selene is not really 3200 hz...


###upsample the gyro data
rFact = 4
targetFreq = 3200 / rFact
dt = rFact / 3200


def interp(old_df, new_df, namex, namey):
    f = interpolate.interp1d(old_df[namex], old_df[namey], kind='cubic')
    new_df[namey] = f(new_df[namex])


df1r = pd.DataFrame(np.arange(0, df1.iloc[-1]['time'], 1 / targetFreq),
                    columns=['time'])
df2r = pd.DataFrame(np.arange(0, df2.iloc[-1]['time2'], 1 / targetFreq),
                    columns=['time2'])

interp(df1, df1r, 'time', 'ax')
interp(df1, df1r, 'time', 'ay')
interp(df1, df1r, 'time', 'az')
interp(df1, df1r, 'time', 'gx')
interp(df1, df1r, 'time', 'gy')
interp(df1, df1r, 'time', 'gz')

###___________________butter accelero______________________
cutoff = 100
freq = 3200
b, a = signal.butter(2, (cutoff / freq), btype='low', analog=False)

df2['az2'] = signal.filtfilt(b, a, df2['az2'])
df2['ax2'] = signal.filtfilt(b, a, df2['ax2'])
df2['ay2'] = signal.filtfilt(b, a, df2['ay2'])
##_______________downsample the data______________________
interp(df2, df2r, 'time2', 'ax2')
interp(df2, df2r, 'time2', 'ay2')
interp(df2, df2r, 'time2', 'az2')

##_____________plot data to set start position___________


plt.figure(2)
plt.plot(df2r['time2'], df2r['az2'], label='azr selene')
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(3)
plt.plot(df1r['time'], df1r['az'], label='azr david')
plt.legend()
plt.draw()
plt.pause(0.001)

## find value for synchronization both samples and intial theta from gravity
plt2 = df2r[df2r['time2'] < 0.3]
plt1 = df1r[df1r['time'] < 0.3]

G_val = ((plt2['az2'] ** 2 + plt2['ax2'] ** 2) ** (1 / 2)).mean()
print("_____________gval")
print(G_val)

w2 = eval(input("maxvalue selene: "))
w1 = eval(input("maxvalue david: "))

print(df2r[df2r['az2'] > w2])
print(df1r[df1r['az'] > w1])

t2 = df2r[df2r['az2'] > w2].iloc[0]['time2']

t1 = df1r[df1r['az'] > w1].iloc[0]['time']
##create merged pandas dataframe with both sources


deltaT = t2-t1



if deltaT > 0:
    df2r['time2'] = df2r['time2'] - deltaT
    df2r.index =  df2r.index - int(deltaT*targetFreq)
else:
    df1r['time'] = df1r['time'] + deltaT
    df1r.index = df1r.index + deltaT * targetFreq
    df2r.index.astype(np.dtype.int64)



df = pd.concat([df1r, df2r], axis=1, join='inner', sort=False)
print(df)


plt.figure(4)
plt.plot(df['time'], df['az2'], label='az selene')
plt.plot(df['time'], df['az'], label='az david')
plt.legend()
plt.show()
plt.pause(0.001)
# axes = plt.gca()
# axes.set_ylim([0,1200])


print(df)

##___________________________done syncing__________________


# ______________GYRO ANGLES and real accel________________________________

# angles estimation using accelero

row = df.iloc[[1]]

# forward
p = 0.002

# def of state
sx = 0
sz = 0
stheta = math.atan2(float(row['ax2']), float(row['az2']))
svx = 0
svz = 0

for i, row in df[1:].iterrows():
    row = df.loc[[i]]
    gy = float(row['gy'])
    az = float(row['az2'])
    ax = float(row['ax2'])
    azr = float(row['az2'])
    axr = float(row['ax2'])

    theta_acc = -math.atan2(axr, azr)
    theta_gyr = stheta + gy * dt

    stheta = (1 - p) * theta_gyr + p * theta_acc

    svx = svx - (math.cos(stheta) * ax + math.sin(stheta) * az) * dt
    svz = svz + (math.sin(stheta) * ax + math.cos(stheta) * az - G_val) * dt

    sx = sx + svx * dt
    sz = sz + svz * dt

    df.loc[[i], 'x'] = sx
    df.loc[[i], 'z'] = sz
    df.loc[[i], 'vx'] = svx
    df.loc[[i], 'vz'] = svz
    df.loc[[i], 'theta'] = stheta
    df.loc[[i], 'theta_acc'] = theta_acc

    print(float(row['time']))

print(df)
plt.figure(5)
plt.plot(df['time'], df['theta'], label='tvstheta')
plt.plot(df['time'], df['theta_acc'], label='tvstheta_acc')
plt.legend()
plt.draw()
plt.pause(0.001)

plt.figure(6)
plt.plot(df['x'], df['z'], label='xvsz')
plt.legend()
plt.show()

plt.close("all")
