import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal

df = pd.read_table('nulpuntsmeting2019-02-25_03-42-21.txt', delimiter=";", header=0)
#df = pd.read_table('2019-02-25_03-11-33.txt', delimiter=";", header=0)

#____________calibration_______________

#ax             0.121568506568460
#ay            -0.000054966195790
#az           -10.933853359534035
#gx            -2.356726986203252
#gy             0.810309998351138
#gz            -0.311074784257679



df['time'] = df['time'] - df['time'][0]  #set t to zero
df['deltatime'] =  df['time'] - df.shift(periods=1)['time'] #calculate time delta (needed for integration

print('std:')
print(df.std())
      

print('mean:')
print(df.mean())      
      
print(df)

df = df[ df['time']>=30] #account for impresions mpu in first 30 seconds
df = df[ df['time']<=35]
fs = 1./(df.mean()["deltatime"])

#print(df)
#pd.set_option('precision', 15)
#print(df.mean())
#pd.set_option('precision', 5)

#adjust for systematic bias (null point reference measurement
df['ax'] = df['ax']- 0.121568506568460
df['ay'] = df['ay']+ 0.000054966195790
df['az'] = df['az']+ 10.933853359534035 - 9.8116
df['wx'] = df['gx']+2.356726986203252
df['wy'] = df['gy']-0.810309998351138
df['wz'] = df['gz']+0.311074784257679



print(df)

#______________GYRO ANGLES and real accel________________________________

# angles estimation using accelero
df['qxa'] = -df['az']/ ((df['ay']**2 + df['az']**2 )** 0.5)
df['qwa'] = df['ay']/((df['ay']**2 + df['az']**2 )** 0.5)

df['aMag'] = (df['az']**2+df['ax']**2+df['ay']**2)**1/2
df['eps'] = abs(df['aMag'] - 9.81)

#init state
row = df.iloc[[1]]
#state = pd.DataFrame( {"qx":[],
#                       "qy":[row['qwa'].values[0]],
#                       "y":[0],
#                       "z":[0],
#                       "vy":[0],
#                       "vz":[0],
#                       "g":[9.8166]} )

state = np.array([row['qxa'].values[0],row['qwa'].values[0],0,0,0,0,9.8166]).T


#forward
p=0.02

for i,row in df[1:].iterrows():
    
    row = df.loc[[i]]
    wx = float(row['wx'])
    az = float(row['az'])
    ay = float(row['ay'])
    dt = float(row['deltatime'])
    qxa = float(row['qxa'])
    qwa = float(row['qwa'])
   
               
    A = np.array( [[1,-wx*dt,0,0,0,0,0],
                   [wx*dt,1,0,0,0,0,0],
                   [0,0,1,0,dt,0,0],
                   [0,0,0,1,0,dt,0],
                   [dt*ay,-dt*az,0,0,1,0,0],
                   [az*dt,ay*dt,0,0,0,1,+dt],
                   [0,0,0,0,0,0,1]]  )
    state = A @ state.T

    theta_model = np.arctan2(state[1],state[0])
    theta_acc = np.arctan2(qwa,qxa)
    
    new_theta = (1-p)* theta_model+p*theta_acc
    
    state[0] = np.cos(new_theta)
    state[1] = np.sin(new_theta)

    df.loc[[i],'y']=   state[2]
    df.loc[[i],'z']=   state[3]
    df.loc[[i],'vy']=state[4]
    df.loc[[i],'vz']=state[5]
    
    df.loc[[i],'theta']= new_theta
    
    
    
print(df)
    
