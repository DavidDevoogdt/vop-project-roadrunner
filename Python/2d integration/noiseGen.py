import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal

plt.figure(1)

for s in range(15):
    tijd = 10
    dt = 0.0083
    num = int(tijd//dt)
    t = np.linspace(0,tijd, num) + np.random.normal(0,0.0099,num)

    gx = np.random.normal(0,0.11,num)
    ax = np.random.normal(0,0.063, num)
    ay = np.random.normal(0,0.009, num)
    az = np.random.normal(-9.81,0.081, num)

    df = pd.DataFrame()
    df['time'] = t
    df['deltatime'] =  df['time'] - df.shift(periods=1)['time']
    df['gx']=gx
    df['az']=az
    df['ay']=ay
    df['ax']=ax
    
    #print(df)
    
    # angles estimation using accelero
    df['qxa'] = -df['az']/ ((df['ay']**2 + df['az']**2 )** 0.5)     #cos theta
    df['qwa'] = df['ay']/((df['ay']**2 + df['az']**2 )** 0.5)       #sin theta



    df['aMag'] = (df['az']**2+df['ax']**2+df['ay']**2)**1/2
    df['eps'] = abs(df['aMag'] - 9.81)

    #init state
    row = df.iloc[[1]]
    #state = pd.DataFrame( {"qx":[],
    #                       "qy":[row['qwa'].values[0]],
    #                       "y":[0],
    #                       "z":[0],
    #                       "vy":[0],
    #                       "vz":[0],
    #                       "g":[9.8166]} )

    state = np.array([row['qxa'].values[0],row['qwa'].values[0],0,0,0,0,9.8166]).T

    print('theta0:%f'%( np.arctan2(state[1],state[0]) ))

    #forward
    p=0.02

    for i,row in df[1:].iterrows():
        
        row = df.loc[[i]]
        wx = float(row['gx'])
        az = float(row['az'])
        ay = float(row['ay'])
        dt = float(row['deltatime'])
        qxa = float(row['qxa'])
        qwa = float(row['qwa'])
    
                
        A = np.array( [[1,-wx*dt,0,0,0,0,0],
                    [wx*dt,1,0,0,0,0,0],
                    [0,0,1,0,dt,0,0],
                    [0,0,0,1,0,dt,0],
                    [dt*ay,-dt*az,0,0,1,0,0],
                    [az*dt,ay*dt,0,0,0,1,+dt],
                    [0,0,0,0,0,0,1]]  )
        state = A @ state.T

        theta_model = np.arctan2(state[1],state[0])
        
        theta_acc = np.arctan2(qwa,qxa)
        
        new_theta = (1-p)* theta_model+p*theta_acc
        
        state[0] = np.cos(new_theta)
        state[1] = np.sin(new_theta)

        df.loc[[i],'y']=   state[2]
        df.loc[[i],'z']=   state[3]
        df.loc[[i],'vy']=state[4]
        df.loc[[i],'vz']=state[5]
        
        df.loc[[i],'theta']= new_theta
        
    plt.plot( df['time'], df['y'])
    
    plt.figure(2)
    plt.plot( df['time'], df['theta'])
    plt.figure(1)

plt.xlabel('tijd [s]')
plt.ylabel('x [m]')
plt.draw()

plt.figure(2)
plt.xlabel('tijd [s]')
plt.ylabel('theta []')
plt.show()


