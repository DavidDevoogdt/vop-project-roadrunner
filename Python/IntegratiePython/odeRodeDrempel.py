 
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.interpolate import interp1d
import csv
kt1=150000
kt2=150000


k1=66000
k2=66000

a1=0.40
a2=0.66

c1=1190*2
c2=1100*2

m=960
m1=11.5
m2=11.5

Iz=-3100





#F=np.array([[0,0],
#            [0,0],
#            [kt1, 0],
#            [0, kt2]])
#K=np.array([[k1+k2,      a2*k2-a1*k1,      -k1,     -k2],
#            [a2*k2-a1*k1, k1*a1**2+k2*a2**2, a1*k1, -a2*k2],
#            [-k1,         a1*k1,             k1*kt1,  0],
#            [-k2,         -a2*k2,            0,       k2+kt2]])
#M=np.array([[ m, 0, 0, 0],
#            [ 0, Iz, 0, 0],
#            [ 0, 0, m1, 0],
#            [ 0, 0, 0, m2]])
#C=np.array([[ c1+c2,      a2*c2-a1*c1,      -c1,   -c2],
#            [a2*c2-a1*c1, c1*a1**2+c2*a2**2, c1*a1, -a2*c2],
#            [ -c1,        a1*c1,             c1,     0],
#            [ -c2,        -a2*c2,             0,    c2]])

####################read height profile
x=[]
y=[]
with open('rode bumper profiel.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        try:
            y.append(-float(row[2].replace(',','.')))
        except:
            0
            
paddingBefore = 40
paddingAfter = 100
x = np.array([0.1*i for i in range(len(y)+paddingBefore+paddingAfter)])
y = np.concatenate( (np.zeros(paddingBefore), np.array(y))  )
y = np.concatenate( (np.array(y),np.zeros(paddingAfter)  ))  
#plt.plot( x, y   , 'b', label='u(t)')
#plt.draw()
#plt.pause(0.001)
#####################


################ read speed profile and sync
df = pd.read_csv('kalmandata.csv',sep= ';')
timeArr = df['seconds'] 
speedArr = df['v_kalman']
excitationArr = y
distanceArr2 = x
distanceArr1 = (0.01*speedArr).cumsum()


interpD2H = interp1d( distanceArr2,excitationArr, kind='linear') 

interpS2D = interp1d(timeArr,distanceArr1,kind = 'linear')   
interpD2S = interp1d(distanceArr1,timeArr,kind = 'linear') 

plt.plot(  df['seconds'] ,df['az0'] , 'b', label='X(t)')
plt.draw()
plt.pause(0.001)


t0 = input('t0 (start time bump) (3.55)= ')
x0 = interpS2D(t0) - paddingBefore*0.1 + a2
t0 = interpD2S(x0)


distanceArr1 -= x0
timeArr -= t0

interpS2D = interp1d(timeArr,distanceArr1,kind = 'linear')   
interpD2S = interp1d(distanceArr1,timeArr,kind = 'linear') 
interpS2Speed = interp1d(timeArr,speedArr,kind = 'linear') 
interpS2A = interp1d(timeArr,df['az0'],kind = 'linear') 


t0 = interpD2S(a2)+0.1
distMax = distanceArr2[-1]
t1 = interpD2S(distMax - a1)-0.1

h0 = interpD2H(a1)

t = np.linspace(t0, t1, 10000)


def u1(t,offset):
    distance = interpS2D(t) 
    height = interpD2H(distance + offset)
    return height




##############
plt.plot( t, u1(t,0)   , 'b', label='u(t)')
plt.plot( t, interpS2A(t)   , 'r', label='u(t)')
plt.draw()
plt.pause(0.001)




####################"



def Var(y, t):
    [x, theta, x1, x2, v, omega, v1, v2] = y

    
    dydt = [v, omega, v1, v2,
            -1/m*(c1*(v-v1-a1*omega)+c2*(v-v2+a2*omega)+k1*(x-x1-a1*theta)+k2*(x-x2+a2*theta)),
            -1/Iz*(a1*c1*(v-v1-a1*omega)-a2*c2*(v-v2+a2*omega)+a1*k1*(x-x1-a1*theta)-a2*k2*(x-x2+a2*theta)),
            -1/m1*(-c1*(v-v1-a1*omega)+kt1*(x1-u1(t,+a1))-k1*(x-x1-a1*theta)),
            -1/m2*(-c2*(v-v2+a2*omega)+kt2*(x2-u1(t,-a2))-k2*(x-x2+a2*theta))]
    return dydt






y0=[h0, 0, h0, h0, 0 , 0, 0, 0]   # [x,theta,x1,x2,v,omega,v1,v2]



sol = odeint(Var, y0, t)




df = pd.DataFrame(sol, columns =['x','theta','x1','x2','v','omega','v1','v2'] )
df['time'] = t
df['laserheight'] = u1(t,0)
df['carspeed'] = interpS2Speed(t)
df['distance_travelled'] = interpS2D(t)


df['az']= -1/m*(c1*(df['v']-df['v1']-a1*df['omega'])+c2*(df['v']-df['v2']+a2*df['omega'])+k1*(df['x']-df['x1']-a1*df['theta'])+k2*(df['x']-df['x2']+a2*df['theta']))
df['az_gemeten'] = -interpS2A(t)+9.81

plt.close()

plt.plot(t, -interpS2A(t)+9.81)
plt.plot( t,df['az'])
plt.show()

df.to_csv('converted.csv', sep=';')

print(df)
plt.close()

#plt.plot(hightframe['seconds'], hightframe['distance_real_m'], label='d vs t')

plt.plot(t, sol[:, 0], 'b', label='X(t)')
plt.plot(t, sol[:, 2], 'y', label='x1(t)')
plt.plot(t, sol[:, 3], label='x2(t)')
plt.plot(t, u1(t,0),'r', label='y(t)')
plt.legend(loc='best')
plt.xlabel('t [s]')
plt.ylabel('h [m]')
plt.grid()
plt.show()
