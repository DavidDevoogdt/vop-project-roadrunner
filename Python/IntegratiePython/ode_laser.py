 
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.interpolate import interp1d
import csv

kt1=300000
kt2=300000


k1=60000
k2=60000

a1=0.40
a2=0.59

c1=3000
c2=3000

m=1000
m1=11.5
m2=11.5

Iz=3000
#F=np.array([[0,0],
#            [0,0],
#            [kt1, 0],
#            [0, kt2]])
#K=np.array([[k1+k2,      a2*k2-a1*k1,      -k1,     -k2],
#            [a2*k2-a1*k1, k1*a1**2+k2*a2**2, a1*k1, -a2*k2],
#            [-k1,         a1*k1,             k1*kt1,  0],
#            [-k2,         -a2*k2,            0,       k2+kt2]])
#M=np.array([[ m, 0, 0, 0],
#            [ 0, Iz, 0, 0],
#            [ 0, 0, m1, 0],
#            [ 0, 0, 0, m2]])
#C=np.array([[ c1+c2,      a2*c2-a1*c1,      -c1,   -c2],
#            [a2*c2-a1*c1, c1*a1**2+c2*a2**2, c1*a1, -a2*c2],
#            [ -c1,        a1*c1,             c1,     0],
#            [ -c2,        -a2*c2,             0,    c2]])


hightframe = pd.read_csv('Converted/E-1_laser.csv',sep= ';')[:-1]
hightframe['distance_real_m'] =hightframe['distance_real_m']/10
hightframe= hightframe[ hightframe['distance_real_m'] <10 ]
hightframe['v_ms']=hightframe['v_kmh']/3.6
hightframe['seconds'] = (0.001/(hightframe['v_ms'])).cumsum()
hightframe['laser_height_m'] = (hightframe['laser_height_mm']-hightframe['laser_height_mm'].iloc[0]  )/1000



timeArr = hightframe['seconds'] 
speedArr = hightframe['v_ms']
excitationArr = hightframe['laser_height_m']
distanceArr = hightframe['distance_real_m']



interpS2D = interp1d(timeArr,distanceArr,kind = 'linear')    #interp seconds to distance real
interpD2H = interp1d( distanceArr,excitationArr, kind='linear')  #interp distance to laserheight
interpD2S = interp1d(distanceArr,timeArr,kind = 'linear') 
interpS2Speed = interp1d(timeArr,speedArr,kind = 'linear') 
interpS2H = interp1d(timeArr,excitationArr,kind = 'linear') 


def u1(t,offset):
    distance = interpS2D(t) 
    height = interpD2H(distance + offset)
    return height



def Var(y, t):
    [x, theta, x1, x2, v, omega, v1, v2] = y

    
    dydt = [v, omega, v1, v2,
            -1/m*(c1*(v-v1-a1*omega)+c2*(v-v2+a2*omega)+k1*(x-x1-a1*theta)+k2*(x-x2+a2*theta)),
            -1/Iz*(a1*c1*(v-v1-a1*omega)-a2*c2*(v-v2+a2*omega)+a1*k1*(x-x1-a1*theta)-a2*k2*(x-x2+a2*theta)),
            -1/m1*(-c1*(v-v1-a1*omega)+kt1*(x1-u1(t,+a1))-k1*(x-x1-a1*theta)),
            -1/m2*(-c2*(v-v2+a2*omega)+kt2*(x2-u1(t,-a2))-k2*(x-x2+a2*theta))]
    return dydt


t0 = interpD2S(a2)+0.1
distMax = distanceArr.iloc[-1]
t1 = interpD2S(distMax - a1)-0.1

h0 = interpS2H(t0)


y0=[h0, 0, h0, h0, 0 , 0, 0, 0]   # [x,theta,x1,x2,v,omega,v1,v2]


t = np.linspace(t0, t1, 500)
sol = odeint(Var, y0, t)






df = pd.DataFrame(sol, columns =['x','theta','x1','x2','v','omega','v1','v2'] )
df['time'] = t
df['laserheight'] = u1(t,0)
df['carspeed'] = interpS2Speed(t)
df['distance_travelled'] = interpS2D(t)

df['az']= -1/m*(c1*(df['v']-df['v1']-a1*df['omega'])+c2*(df['v']-df['v2']+a2*df['omega'])+k1*(df['x']-df['x1']-a1*df['theta'])+k2*(df['x']-df['x2']+a2*df['theta']))

df.to_csv('converted.csv', sep=';')

print(df)
plt.close()

#plt.plot(hightframe['seconds'], hightframe['distance_real_m'], label='d vs t')

plt.plot(t, sol[:, 0], 'b', label='X(t)')
plt.plot(t, sol[:, 2], 'y', label='x1(t)')
plt.plot(t, sol[:, 3], label='x2(t)')
plt.plot(t, u1(t,0),'r', label='y(t)')
plt.legend(loc='best')
plt.xlabel('t [s]')
plt.ylabel('h [m]')
plt.grid()
plt.show()
