##https://github.com/balzer82/Kalman


import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm



x = np.matrix([[0.0, 0.0, 0.0, 0.0, 0.0, 0.0]]).T #initial state matrix # x y vx vy ax ay

P = np.diag([100.0, 100.0, 10.0, 10.0, 1.0, 1.0]) #initial uncertainity


dt = 0.1
A = np.matrix([[1.0, 0.0, dt, 0.0, 1/2.0*dt**2, 0.0],
              [0.0, 1.0, 0.0, dt, 0.0, 1/2.0*dt**2],
              [0.0, 0.0, 1.0, 0.0, dt, 0.0],
              [0.0, 0.0, 0.0, 1.0, 0.0, dt],
              [0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
              [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]]) #dynamics matrix

ra = 10.0**2   
rp = 100.0**2 
### No gps
H1 = np.matrix([[0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
              [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])   #measurement matrix

R1 = np.matrix([[ra, 0.0],
               [0.0, ra]])    # measurement noise covariance

### with GPS
H2 = np.matrix([[1.0, 0.0, 0.0, 0.0, 0.0, 0.0],
               [0.0, 1.0, 0.0, 0.0, 0.0, 0.0],
               [0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
              [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])  #measurement matrix

R2 = np.matrix([[rp, 0.0, 0.0, 0.0],
               [0.0, rp, 0.0, 0.0],
               [0.0, 0.0, ra, 0.0],
               [0.0, 0.0, 0.0, ra]])# measurement noise covariance

### Process Noise Covariance Matrix 
sa = 0.001
G = np.matrix([[1/2.0*dt**2],
               [1/2.0*dt**2],
               [dt],
               [dt],
               [1.0],
               [1.0]])
Q = G*G.T*sa**2

I = np.eye(6)

#################### end setup,generating measurements
m = 500 # Measurements

time = 500*dt

sp= 1.0 # Sigma for position
sa= 0.1 # Sigma for acc

numArr = np.linspace(0,m,1)
tArr = numArr*dt

px= np.cos( tArr*2*3.14/time ) # x Position
py= np.sin( tArr*2*3.14/time ) # y Position

ax =  - (2*3.14/time)**(2) * np.cos( tArr*2*3.14/m )
ay =  - (2*3.14/time)**(2) * np.sin( tArr*2*3.14/m )

mxp = px +  sp*np.random.randn(m)
myp = py +  sp*np.random.randn(m)
mxa = ax +  sa*np.random.randn(m)
mya = ay +  sa*np.random.randn(m)

measurements = np.vstack((mxp,myp,mxa,mya))

############### start kalman

xPredicted = []


def predict(A,x,P):
    x = A*x  # Project the state ahead
    P = A*P*A.T + Q # Project the error covariance ahead
    return (x,P)
def correct(H,P,R,M,x):
    K = (P*H.T) * np.linalg.pinv( H*P*H.T + R)  # Compute the Kalman Gain
    x = x + K*(M - (H*x)) # Update the estimate via M
    P = (I - (K*H))*P # Update the error covariance
    return x,P

for filterstep in range(m):
    x,P = predict(A,x,P)

    if m % 10:
        x,P = correct(H2,P,R2, measurements[:,filterstep].reshape(4,1),x ) #accerleration and gps data
    else : 
        x,P = correct(H1,P,R1, measurements[0:2,filterstep].reshape(2,1),x )  #only accerleration data
        
    xPredicted.append(x)

##todo: do 2d plot of variable calculated and exact


