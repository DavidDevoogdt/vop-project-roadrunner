 
 
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import signal, interpolate
import matplotlib.colors as colors


def running_mean(x, N):
    y_padded = np.pad(x, (N//2, N-1-N//2), mode='edge')
    y_smooth = np.convolve(y_padded, np.ones((N,))/N, mode='valid') 
    
    return y_smooth

acceldata1 = pd.read_csv('Laser_hight_model.csv',sep= ';')

xData = acceldata1['time']
yData1 = acceldata1['az']
yData2 = acceldata1['laserheight']
deltaX= (xData.iloc[-1] - xData.iloc[0])/xData.count()

print(deltaX)

chunckSize = 0.1

totalSize = float(xData.iloc[-1])
numRows = int(xData.count())

fft1 = np.fft.rfft( yData1 )
fft2= np.fft.rfft(  yData2  )


x= np.linspace(0, 1/(deltaX), numRows//2+1)

plt.figure(0)
plt.plot( x,fft1  )


plt.figure(1)
plt.plot( x,fft2 ) 


plt.figure(2)


h = (fft1/fft2)
h2 = running_mean(h,10)


print(h)
plt.plot( x, np.absolute( h2) , label='amplitude'  ) 
#plt.plot( x, h2.real , label='real'  ) 

plt.yscale('log')
plt.xlabel('Hoekfrequentie ω [Hz]')
plt.ylabel(' |H_car| [1/s^2]')
plt.xlim([0,100])
#plt.figure(3)
#plt.plot( x, h2.imag , label='real'  ) 

#plt.yscale('log')
#plt.xlabel('Hoekfrequentie ω [Hz]')
#plt.ylabel('amplitude [1/s^2]')


plt.figure(3)

 
h = (fft1/fft2)
plt.plot( x,np.unwrap(np.angle(h2)), label='arg(H_car)' ) 

plt.xlim([0,100])
plt.ylabel('arg(H_car) []')
plt.xlabel('Hoekfrequentie ω [Hz]')



plt.show()
