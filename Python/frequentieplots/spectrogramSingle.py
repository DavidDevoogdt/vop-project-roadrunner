 
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import signal, interpolate
import matplotlib.colors as colors

acceldata1 = pd.read_csv('Laser_hight_model.csv',sep= ';')

print(acceldata1)

xData = acceldata1['time']
yData1 = acceldata1['az']
chunckSize = 2


###########"
totalSize = float(xData.iloc[-1])
chunks = int(totalSize//chunckSize)
numRows = int(xData.count())
rowsPerChunk = numRows//chunks
deltaX= (xData.iloc[-1] - xData.iloc[0])/xData.count()
#############

map2d1 = []
map2d2 = []

for i in range(0,chunks):
    part1 = yData1.iloc[i*rowsPerChunk :(i+1)*rowsPerChunk]
    fft1 = np.fft.rfft( part1 )
    map2d1.append( fft1 ) 


image1 = 1/rowsPerChunk * np.array(map2d1) .T
image2 = 1/rowsPerChunk * np.array(map2d2) .T

image1 = abs( image1.real) #(((image1*np.conjugate(image1) )** 0.5) .real)
image2 = abs(image2.real) #(((image2*np.conjugate(image2) )** 0.5) .real)

x= np.linspace(0, totalSize, chunks)
y= np.linspace(0, 1/(deltaX), rowsPerChunk//2+1)
X,Y=np.meshgrid(x,y)

plt.figure(0)
plt.pcolormesh( X,Y,image1 ,norm=colors.LogNorm(10**(-3), vmax=10**3),)  
plt.ylabel('Hoekfrequentie ω [Hz]')
plt.xlabel('tijd [s]')
plt.colorbar()
plt.yscale('log')
plt.ylim(10**(0),10**3)

plt.yscale('log')

plt.show()
