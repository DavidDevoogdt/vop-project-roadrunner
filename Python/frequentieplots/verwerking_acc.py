 
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import signal, interpolate
import matplotlib.colors as colors

acceldata = pd.read_csv('Converted/trip5_F_1.csv',sep= ';')


xData = acceldata['seconds']
yData = acceldata['az'].values*9.81-np.mean(acceldata['az'])*9.81
chunckSize = 2.5


###########"
totalSize = float(xData.iloc[-1])
chunks = int(totalSize//chunckSize)
numRows = int(yData.count())
rowsPerChunk = numRows//chunks
deltaX= (xData.iloc[-1] - xData.iloc[0])/xData.count
#############

map2d = []

for i in range(0,chunks):
    
    part = yData.iloc[i*rowsPerChunk :(i+1)*rowsPerChunk]
    fft = np.fft.rfft( part  )
    map2d.append( fft ) 


image = 1/rowsPerChunk * np.array(map2d) .T
image = (((image*np.conjugate(image) )** 0.5) .real)

x= np.linspace(0, totalSize, chunks)
y= np.linspace(0, 1/(deltaX), rowsPerChunk//2+1)
X,Y=np.meshgrid(x,y)

plt.pcolormesh( X,Y,image ,norm=colors.LogNorm(10**(-3), vmax=10**2),)  
plt.colorbar()
plt.yscale('log')
plt.ylim(10**(-1),10**2)

plt.show()
