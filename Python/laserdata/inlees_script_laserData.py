import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os

#D-1 E-1 F-1 G1
pathN = 'dataset/laser/'
Name = 'G-1'

contents = pd.read_csv( pathN  + Name + '.txt',sep="	")


contents['distance_real_m'] = (contents['interdistance_mm'].cumsum(axis=0))/100

contents.to_csv('Converted/'+ Name +'_laser.csv', sep=';')


