 
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import signal, interpolate
from matplotlib.image import NonUniformImage
import matplotlib.colors as colors

hightdata = pd.read_csv('Converted/F-1_laser.csv',sep= ';')[:-1]
hightdata['v_ms']=hightdata['v_kmh']/3.6



hightdata['distance_real_m'] = hightdata['distance_real_m']/10 #foutje in inleesscript

totalLenght = float(hightdata['distance_real_m'].iloc[-1])
chunkLenght = 20
chunks = int(totalLenght//chunkLenght)
numRows = int(hightdata['laser_height_mm'].count())
rowsPerChunk = numRows//chunks

dx=0.001


map2d = []

y= np.linspace(0, 1/(dx), rowsPerChunk//2+1)

for i in range(0,chunks):
    
    
    #%matplotlib inline
    #import numpy as np
    #import matplotlib.pyplot as plt
    #import scipy.fftpack

    #Number of samplepoints
    #N = 600
    #sample spacing
    #T = 1.0 / 800.0
    #x = np.linspace(0.0, N*T, N)
    #y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)
    #yf = scipy.fftpack.fft(y)
    #xf = np.linspace(0.0, 1.0/(2.0*T), N/2)

    #fig, ax = plt.subplots()
    #ax.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    #plt.show()
    
    
    part = hightdata.iloc[i*rowsPerChunk :(i+1)*rowsPerChunk]
    fft = np.fft.rfft( part['laser_height_mm']  )
    
    
    
    map2d.append( fft ) 
    #map2d.append( fft*y**2*4*(np.pi)**2 ) 
    print(fft)
    #print(fft)
    #plt.plot(y,abs(fft))
    #plt.show()
    #input()
    


image = 1/rowsPerChunk * np.array(map2d) .T

image = (((image*np.conjugate(image) )** 0.5) .real)



x= np.linspace(0, totalLenght, chunks)

X,Y=np.meshgrid(x,y)

plt.pcolormesh( X,Y,image ,norm=colors.LogNorm(10**(-3), vmax=10**2),) 
plt.colorbar( extend='max')
plt.yscale('log')
plt.ylim(10**(-1),10**2)
#fftfreq = np.fft.fftfreq(part['distance_real_m'])

plt.show()
