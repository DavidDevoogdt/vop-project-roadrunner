# -*- coding: utf-8 -*-
"""
Created on Sun May  5 14:36:17 2019

@author: keano
"""
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import signal, interpolate


acceldata = pd.read_csv('trip4_E_1.csv',sep= ';')
hightdata = pd.read_csv('E-1_laser.csv',sep= ';')[:-1]
hightdata['v_ms']=hightdata['v_kmh']/3.6

hightdata['time2']=(0.001/hightdata['v_ms']).cumsum()
print(hightdata['time2'])
def butterfilter(data):
    b, a = signal.butter(4, 0.5)
    y = signal.filtfilt(b, a, data)
    return y

acceldata['azfilt'] = butterfilter(acceldata['az'].values)
f = interpolate.interp1d( hightdata['time2'].values,hightdata['laser_height_mm'].values/1000,"cubic")
x = np.linspace(0.01,68.86,68860)
print(f(x))
hightfft = np.fft.fft(f(x)-np.mean(f(x)))
hightfreq = np.fft.fftfreq(68860,d=0.001)
hightomegafft = hightfft*hightfreq**2*2*np.pi*2*np.pi
accelfft=np.fft.fft(acceldata['az'].values*9.81-np.mean(acceldata['az'])*9.81)
accelfreq=np.fft.fftfreq(4400,0.01)
accelomegafft = accelfft
print(hightfft)
plt.close(1)
plt.figure(1)
plt.plot( acceldata['seconds'], acceldata['az'], label="az oorspronkelijk")
plt.plot( acceldata['seconds'], acceldata['azfilt'], label="azbutter")
plt.legend()
plt.draw()
plt.close(2)
plt.figure(2)
plt.plot( hightfreq[((-1<hightfreq) & (hightfreq<1))],abs(hightomegafft[((-1<hightfreq) & (hightfreq<1))]), label="hoogte gecorrigeerd nr snelheid")
plt.plot( accelfreq[((-1<accelfreq) & (accelfreq<1))],abs(accelomegafft[((-1<accelfreq) & (accelfreq<1))]), label="versnelling gecorrigeerd nr snelheid")
plt.legend()
plt.draw()
plt.close(3)
plt.figure(3)
plt.plot( x, f(x), label="hight oorspronkelijk")
plt.legend()
plt.draw()