 
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
from scipy import signal, interpolate
from matplotlib.image import NonUniformImage
import matplotlib.colors as colors

acceldata = pd.read_csv('Converted/trip5_F_1.csv',sep= ';')

acceldata['az2'] = acceldata['az'].values*9.81-np.mean(acceldata['az'])*9.81

totalTime = float(acceldata['seconds'].iloc[-1])
chunkTime = 2.5 #todo fix with real lenght
chunks = int(totalTime//chunkTime)
numRows = int(acceldata['az2'].count())
rowsPerChunk = numRows//chunks

dt=0.01

map2d = []

for i in range(0,chunks):
    
    
    #%matplotlib inline
    #import numpy as np
    #import matplotlib.pyplot as plt
    #import scipy.fftpack

    #Number of samplepoints
    #N = 600
    #sample spacing
    #T = 1.0 / 800.0
    #x = np.linspace(0.0, N*T, N)
    #y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)
    #yf = scipy.fftpack.fft(y)
    #xf = np.linspace(0.0, 1.0/(2.0*T), N/2)

    #fig, ax = plt.subplots()
    #ax.plot(xf, 2.0/N * np.abs(yf[:N//2]))
    #plt.show()
    
    
    part = acceldata.iloc[i*rowsPerChunk :(i+1)*rowsPerChunk]
    fft = np.fft.rfft( part['az2']  )
    
    
    
    map2d.append( fft ) 
    print(fft)
    #print(fft)
    #plt.plot(y,abs(fft))
    #plt.show()
    #input()
    


image = 1/rowsPerChunk * np.array(map2d) .T

image = (((image*np.conjugate(image) )** 0.5) .real)

x= np.linspace(0, totalTime, chunks)
y= np.linspace(0, 1/(dt), rowsPerChunk//2+1)
X,Y=np.meshgrid(x,y)

plt.pcolormesh( X,Y,image ,norm=colors.LogNorm(10**(-3), vmax=10**2),)  
plt.colorbar( extend='max')
plt.yscale('log')
plt.ylim(10**(-1),10**2)
#fftfreq = np.fft.fftfreq(part['distance_real_m'])

plt.show()
