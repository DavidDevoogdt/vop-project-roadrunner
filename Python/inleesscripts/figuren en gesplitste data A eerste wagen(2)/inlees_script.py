import pandas as pd
import numpy as np

contents = pd.read_csv('dataset_speed_bump_zwijnaarde.csv')

def split_by_trip(df):
    splits = {}
    for trip_id in df.trip_id.unique():
        split = df[df['trip_id'] == trip_id]
        splits[trip_id] = split
    return splits
splits = split_by_trip(contents)

for trip_id in splits:
    contents = splits[trip_id][['a_xyz_1sec-y0','a_xyz_1sec-z0','a_xyz_1sec-x1','a_xyz_1sec-y1','a_xyz_1sec-z1','a_xyz_1sec-x2','a_xyz_1sec-y2','a_xyz_1sec-z2','a_xyz_1sec-x3','a_xyz_1sec-y3','a_xyz_1sec-z3','a_xyz_1sec-x4','a_xyz_1sec-y4','a_xyz_1sec-z4','a_xyz_1sec-x5','a_xyz_1sec-y5','a_xyz_1sec-z5','a_xyz_1sec-x6','a_xyz_1sec-y6','a_xyz_1sec-z6','a_xyz_1sec-x7','a_xyz_1sec-y7','a_xyz_1sec-z7','a_xyz_1sec-x8','a_xyz_1sec-y8','a_xyz_1sec-z8','a_xyz_1sec-x9','a_xyz_1sec-y9','a_xyz_1sec-z9','a_xyz_1sec-x10','a_xyz_1sec-y10','a_xyz_1sec-z10','a_xyz_1sec-x11','a_xyz_1sec-y11','a_xyz_1sec-z11','a_xyz_1sec-x12','a_xyz_1sec-y12','a_xyz_1sec-z12','a_xyz_1sec-x13','a_xyz_1sec-y13','a_xyz_1sec-z13','a_xyz_1sec-x14','a_xyz_1sec-y14','a_xyz_1sec-z14','a_xyz_1sec-x15','a_xyz_1sec-y15','a_xyz_1sec-z15','a_xyz_1sec-x16','a_xyz_1sec-y16','a_xyz_1sec-z16','a_xyz_1sec-x17','a_xyz_1sec-y17','a_xyz_1sec-z17','a_xyz_1sec-x18','a_xyz_1sec-y18','a_xyz_1sec-z18','a_xyz_1sec-x19','a_xyz_1sec-y19','a_xyz_1sec-z19','a_xyz_1sec-x20','a_xyz_1sec-y20','a_xyz_1sec-z20','a_xyz_1sec-x21','a_xyz_1sec-y21','a_xyz_1sec-z21','a_xyz_1sec-x22','a_xyz_1sec-y22','a_xyz_1sec-z22','a_xyz_1sec-x23','a_xyz_1sec-y23','a_xyz_1sec-z23','a_xyz_1sec-x24','a_xyz_1sec-y24','a_xyz_1sec-z24','a_xyz_1sec-x25','a_xyz_1sec-y25','a_xyz_1sec-z25','a_xyz_1sec-x26','a_xyz_1sec-y26','a_xyz_1sec-z26','a_xyz_1sec-x27','a_xyz_1sec-y27','a_xyz_1sec-z27','a_xyz_1sec-x28','a_xyz_1sec-y28','a_xyz_1sec-z28','a_xyz_1sec-x29','a_xyz_1sec-y29','a_xyz_1sec-z29','a_xyz_1sec-x30','a_xyz_1sec-y30','a_xyz_1sec-z30','a_xyz_1sec-x31','a_xyz_1sec-y31','a_xyz_1sec-z31','a_xyz_1sec-x32','a_xyz_1sec-y32','a_xyz_1sec-z32','a_xyz_1sec-x33','a_xyz_1sec-y33','a_xyz_1sec-z33','a_xyz_1sec-x34','a_xyz_1sec-y34','a_xyz_1sec-z34','a_xyz_1sec-x35','a_xyz_1sec-y35','a_xyz_1sec-z35','a_xyz_1sec-x36','a_xyz_1sec-y36','a_xyz_1sec-z36','a_xyz_1sec-x37','a_xyz_1sec-y37','a_xyz_1sec-z37','a_xyz_1sec-x38','a_xyz_1sec-y38','a_xyz_1sec-z38','a_xyz_1sec-x39','a_xyz_1sec-y39','a_xyz_1sec-z39','a_xyz_1sec-x40','a_xyz_1sec-y40','a_xyz_1sec-z40','a_xyz_1sec-x41','a_xyz_1sec-y41','a_xyz_1sec-z41','a_xyz_1sec-x42','a_xyz_1sec-y42','a_xyz_1sec-z42','a_xyz_1sec-x43','a_xyz_1sec-y43','a_xyz_1sec-z43','a_xyz_1sec-x44','a_xyz_1sec-y44','a_xyz_1sec-z44','a_xyz_1sec-x45','a_xyz_1sec-y45','a_xyz_1sec-z45','a_xyz_1sec-x46','a_xyz_1sec-y46','a_xyz_1sec-z46','a_xyz_1sec-x47','a_xyz_1sec-y47','a_xyz_1sec-z47','a_xyz_1sec-x48','a_xyz_1sec-y48','a_xyz_1sec-z48','a_xyz_1sec-x49','a_xyz_1sec-y49','a_xyz_1sec-z49','a_xyz_1sec-x50','a_xyz_1sec-y50','a_xyz_1sec-z50','a_xyz_1sec-x51','a_xyz_1sec-y51','a_xyz_1sec-z51','a_xyz_1sec-x52','a_xyz_1sec-y52','a_xyz_1sec-z52','a_xyz_1sec-x53','a_xyz_1sec-y53','a_xyz_1sec-z53','a_xyz_1sec-x54','a_xyz_1sec-y54','a_xyz_1sec-z54','a_xyz_1sec-x55','a_xyz_1sec-y55','a_xyz_1sec-z55','a_xyz_1sec-x56','a_xyz_1sec-y56','a_xyz_1sec-z56','a_xyz_1sec-x57','a_xyz_1sec-y57','a_xyz_1sec-z57','a_xyz_1sec-x58','a_xyz_1sec-y58','a_xyz_1sec-z58','a_xyz_1sec-x59','a_xyz_1sec-y59','a_xyz_1sec-z59','a_xyz_1sec-x60','a_xyz_1sec-y60','a_xyz_1sec-z60','a_xyz_1sec-x61','a_xyz_1sec-y61','a_xyz_1sec-z61','a_xyz_1sec-x62','a_xyz_1sec-y62','a_xyz_1sec-z62','a_xyz_1sec-x63','a_xyz_1sec-y63','a_xyz_1sec-z63','a_xyz_1sec-x64','a_xyz_1sec-y64','a_xyz_1sec-z64','a_xyz_1sec-x65','a_xyz_1sec-y65','a_xyz_1sec-z65','a_xyz_1sec-x66','a_xyz_1sec-y66','a_xyz_1sec-z66','a_xyz_1sec-x67','a_xyz_1sec-y67','a_xyz_1sec-z67','a_xyz_1sec-x68','a_xyz_1sec-y68','a_xyz_1sec-z68','a_xyz_1sec-x69','a_xyz_1sec-y69','a_xyz_1sec-z69','a_xyz_1sec-x70','a_xyz_1sec-y70','a_xyz_1sec-z70','a_xyz_1sec-x71','a_xyz_1sec-y71','a_xyz_1sec-z71','a_xyz_1sec-x72','a_xyz_1sec-y72','a_xyz_1sec-z72','a_xyz_1sec-x73','a_xyz_1sec-y73','a_xyz_1sec-z73','a_xyz_1sec-x74','a_xyz_1sec-y74','a_xyz_1sec-z74','a_xyz_1sec-x75','a_xyz_1sec-y75','a_xyz_1sec-z75','a_xyz_1sec-x76','a_xyz_1sec-y76','a_xyz_1sec-z76','a_xyz_1sec-x77','a_xyz_1sec-y77','a_xyz_1sec-z77','a_xyz_1sec-x78','a_xyz_1sec-y78','a_xyz_1sec-z78','a_xyz_1sec-x79','a_xyz_1sec-y79','a_xyz_1sec-z79','a_xyz_1sec-x80','a_xyz_1sec-y80','a_xyz_1sec-z80','a_xyz_1sec-x81','a_xyz_1sec-y81','a_xyz_1sec-z81','a_xyz_1sec-x82','a_xyz_1sec-y82','a_xyz_1sec-z82','a_xyz_1sec-x83','a_xyz_1sec-y83','a_xyz_1sec-z83','a_xyz_1sec-x84','a_xyz_1sec-y84','a_xyz_1sec-z84','a_xyz_1sec-x85','a_xyz_1sec-y85','a_xyz_1sec-z85','a_xyz_1sec-x86','a_xyz_1sec-y86','a_xyz_1sec-z86','a_xyz_1sec-x87','a_xyz_1sec-y87','a_xyz_1sec-z87','a_xyz_1sec-x88','a_xyz_1sec-y88','a_xyz_1sec-z88','a_xyz_1sec-x89','a_xyz_1sec-y89','a_xyz_1sec-z89','a_xyz_1sec-x90','a_xyz_1sec-y90','a_xyz_1sec-z90','a_xyz_1sec-x91','a_xyz_1sec-y91','a_xyz_1sec-z91','a_xyz_1sec-x92','a_xyz_1sec-y92','a_xyz_1sec-z92','a_xyz_1sec-x93','a_xyz_1sec-y93','a_xyz_1sec-z93','a_xyz_1sec-x94','a_xyz_1sec-y94','a_xyz_1sec-z94','a_xyz_1sec-x95','a_xyz_1sec-y95','a_xyz_1sec-z95','a_xyz_1sec-x96','a_xyz_1sec-y96','a_xyz_1sec-z96','a_xyz_1sec-x97','a_xyz_1sec-y97','a_xyz_1sec-z97','a_xyz_1sec-x98','a_xyz_1sec-y98','a_xyz_1sec-z98','a_xyz_1sec-x99','a_xyz_1sec-y99','a_xyz_1sec-z99']]
    accel_x = contents.values[:,::3].flatten()
    accel_y = contents.values[:,1::3].flatten()
    accel_z = contents.values[:,2::3].flatten()
    l = min(len(accel_x), len(accel_y), len(accel_z))
    
    accel_x = accel_x[:l]
    accel_y = accel_y[:l]
    accel_z = accel_z[:l]
    t = np.arange(len(accel_x)) / 50.0
    print()
    print(trip_id)
    #print(contents)
    print(len(accel_x))
    print(len(accel_y))
    print(len(accel_z))
    
    import matplotlib.pyplot as plt
    plt.subplots()
    plt.plot(accel_z)
    plt.title(str(trip_id))
    plt.savefig(str(trip_id) + '.png')
    pd.DataFrame({'a_x': accel_x, 'a_y': accel_y, 'a_z': accel_z}).to_csv(str(trip_id) + '.csv')
    
    ## do something with accel_x, accel_y, accel_z
