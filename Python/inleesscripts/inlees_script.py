import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import os


pathN = 'speed bumps/data/C'

contents = pd.read_csv( pathN  + '.csv')

def split_by_trip(df):
    splits = {}
    for trip_id in df.trip_id.unique():
        split = df[df['trip_id'] == trip_id]
        splits[trip_id] = split
    return splits
splits = split_by_trip(contents)

for trip_id in splits:
    contents = splits[trip_id][['time','GPSau-class','GPSau-device','GPSau-status','GPSau-mode','GPSau-time','GPSau-ept','GPSau-lat','GPSau-lon','GPSau-alt','GPSau-epx','GPSau-epy','GPSau-epv','GPSau-track','GPSau-speed','GPSau-climb','GPSau-epd','GPSau-eps','GPSau-epc','matched_gps_wout-segment_id','matched_gps_wout-snap_lon','matched_gps_wout-snap_lat','matched_gps_wout-segment_from_lon','matched_gps_wout-segment_from_lat','matched_gps_wout-segment_to_lon','matched_gps_wout-segment_to_lat','a_xyz_1sec-x0','a_xyz_1sec-y0','a_xyz_1sec-z0','a_xyz_1sec-x1','a_xyz_1sec-y1','a_xyz_1sec-z1','a_xyz_1sec-x2','a_xyz_1sec-y2','a_xyz_1sec-z2','a_xyz_1sec-x3','a_xyz_1sec-y3','a_xyz_1sec-z3','a_xyz_1sec-x4','a_xyz_1sec-y4','a_xyz_1sec-z4','a_xyz_1sec-x5','a_xyz_1sec-y5','a_xyz_1sec-z5','a_xyz_1sec-x6','a_xyz_1sec-y6','a_xyz_1sec-z6','a_xyz_1sec-x7','a_xyz_1sec-y7','a_xyz_1sec-z7','a_xyz_1sec-x8','a_xyz_1sec-y8','a_xyz_1sec-z8','a_xyz_1sec-x9','a_xyz_1sec-y9','a_xyz_1sec-z9','a_xyz_1sec-x10','a_xyz_1sec-y10','a_xyz_1sec-z10','a_xyz_1sec-x11','a_xyz_1sec-y11','a_xyz_1sec-z11','a_xyz_1sec-x12','a_xyz_1sec-y12','a_xyz_1sec-z12','a_xyz_1sec-x13','a_xyz_1sec-y13','a_xyz_1sec-z13','a_xyz_1sec-x14','a_xyz_1sec-y14','a_xyz_1sec-z14','a_xyz_1sec-x15','a_xyz_1sec-y15','a_xyz_1sec-z15','a_xyz_1sec-x16','a_xyz_1sec-y16','a_xyz_1sec-z16','a_xyz_1sec-x17','a_xyz_1sec-y17','a_xyz_1sec-z17','a_xyz_1sec-x18','a_xyz_1sec-y18','a_xyz_1sec-z18','a_xyz_1sec-x19','a_xyz_1sec-y19','a_xyz_1sec-z19','a_xyz_1sec-x20','a_xyz_1sec-y20','a_xyz_1sec-z20','a_xyz_1sec-x21','a_xyz_1sec-y21','a_xyz_1sec-z21','a_xyz_1sec-x22','a_xyz_1sec-y22','a_xyz_1sec-z22','a_xyz_1sec-x23','a_xyz_1sec-y23','a_xyz_1sec-z23','a_xyz_1sec-x24','a_xyz_1sec-y24','a_xyz_1sec-z24','a_xyz_1sec-x25','a_xyz_1sec-y25','a_xyz_1sec-z25','a_xyz_1sec-x26','a_xyz_1sec-y26','a_xyz_1sec-z26','a_xyz_1sec-x27','a_xyz_1sec-y27','a_xyz_1sec-z27','a_xyz_1sec-x28','a_xyz_1sec-y28','a_xyz_1sec-z28','a_xyz_1sec-x29','a_xyz_1sec-y29','a_xyz_1sec-z29','a_xyz_1sec-x30','a_xyz_1sec-y30','a_xyz_1sec-z30','a_xyz_1sec-x31','a_xyz_1sec-y31','a_xyz_1sec-z31','a_xyz_1sec-x32','a_xyz_1sec-y32','a_xyz_1sec-z32','a_xyz_1sec-x33','a_xyz_1sec-y33','a_xyz_1sec-z33','a_xyz_1sec-x34','a_xyz_1sec-y34','a_xyz_1sec-z34','a_xyz_1sec-x35','a_xyz_1sec-y35','a_xyz_1sec-z35','a_xyz_1sec-x36','a_xyz_1sec-y36','a_xyz_1sec-z36','a_xyz_1sec-x37','a_xyz_1sec-y37','a_xyz_1sec-z37','a_xyz_1sec-x38','a_xyz_1sec-y38','a_xyz_1sec-z38','a_xyz_1sec-x39','a_xyz_1sec-y39','a_xyz_1sec-z39','a_xyz_1sec-x40','a_xyz_1sec-y40','a_xyz_1sec-z40','a_xyz_1sec-x41','a_xyz_1sec-y41','a_xyz_1sec-z41','a_xyz_1sec-x42','a_xyz_1sec-y42','a_xyz_1sec-z42','a_xyz_1sec-x43','a_xyz_1sec-y43','a_xyz_1sec-z43','a_xyz_1sec-x44','a_xyz_1sec-y44','a_xyz_1sec-z44','a_xyz_1sec-x45','a_xyz_1sec-y45','a_xyz_1sec-z45','a_xyz_1sec-x46','a_xyz_1sec-y46','a_xyz_1sec-z46','a_xyz_1sec-x47','a_xyz_1sec-y47','a_xyz_1sec-z47','a_xyz_1sec-x48','a_xyz_1sec-y48','a_xyz_1sec-z48','a_xyz_1sec-x49','a_xyz_1sec-y49','a_xyz_1sec-z49','a_xyz_1sec-x50','a_xyz_1sec-y50','a_xyz_1sec-z50','a_xyz_1sec-x51','a_xyz_1sec-y51','a_xyz_1sec-z51','a_xyz_1sec-x52','a_xyz_1sec-y52','a_xyz_1sec-z52','a_xyz_1sec-x53','a_xyz_1sec-y53','a_xyz_1sec-z53','a_xyz_1sec-x54','a_xyz_1sec-y54','a_xyz_1sec-z54','a_xyz_1sec-x55','a_xyz_1sec-y55','a_xyz_1sec-z55','a_xyz_1sec-x56','a_xyz_1sec-y56','a_xyz_1sec-z56','a_xyz_1sec-x57','a_xyz_1sec-y57','a_xyz_1sec-z57','a_xyz_1sec-x58','a_xyz_1sec-y58','a_xyz_1sec-z58','a_xyz_1sec-x59','a_xyz_1sec-y59','a_xyz_1sec-z59','a_xyz_1sec-x60','a_xyz_1sec-y60','a_xyz_1sec-z60','a_xyz_1sec-x61','a_xyz_1sec-y61','a_xyz_1sec-z61','a_xyz_1sec-x62','a_xyz_1sec-y62','a_xyz_1sec-z62','a_xyz_1sec-x63','a_xyz_1sec-y63','a_xyz_1sec-z63','a_xyz_1sec-x64','a_xyz_1sec-y64','a_xyz_1sec-z64','a_xyz_1sec-x65','a_xyz_1sec-y65','a_xyz_1sec-z65','a_xyz_1sec-x66','a_xyz_1sec-y66','a_xyz_1sec-z66','a_xyz_1sec-x67','a_xyz_1sec-y67','a_xyz_1sec-z67','a_xyz_1sec-x68','a_xyz_1sec-y68','a_xyz_1sec-z68','a_xyz_1sec-x69','a_xyz_1sec-y69','a_xyz_1sec-z69','a_xyz_1sec-x70','a_xyz_1sec-y70','a_xyz_1sec-z70','a_xyz_1sec-x71','a_xyz_1sec-y71','a_xyz_1sec-z71','a_xyz_1sec-x72','a_xyz_1sec-y72','a_xyz_1sec-z72','a_xyz_1sec-x73','a_xyz_1sec-y73','a_xyz_1sec-z73','a_xyz_1sec-x74','a_xyz_1sec-y74','a_xyz_1sec-z74','a_xyz_1sec-x75','a_xyz_1sec-y75','a_xyz_1sec-z75','a_xyz_1sec-x76','a_xyz_1sec-y76','a_xyz_1sec-z76','a_xyz_1sec-x77','a_xyz_1sec-y77','a_xyz_1sec-z77','a_xyz_1sec-x78','a_xyz_1sec-y78','a_xyz_1sec-z78','a_xyz_1sec-x79','a_xyz_1sec-y79','a_xyz_1sec-z79','a_xyz_1sec-x80','a_xyz_1sec-y80','a_xyz_1sec-z80','a_xyz_1sec-x81','a_xyz_1sec-y81','a_xyz_1sec-z81','a_xyz_1sec-x82','a_xyz_1sec-y82','a_xyz_1sec-z82','a_xyz_1sec-x83','a_xyz_1sec-y83','a_xyz_1sec-z83','a_xyz_1sec-x84','a_xyz_1sec-y84','a_xyz_1sec-z84','a_xyz_1sec-x85','a_xyz_1sec-y85','a_xyz_1sec-z85','a_xyz_1sec-x86','a_xyz_1sec-y86','a_xyz_1sec-z86','a_xyz_1sec-x87','a_xyz_1sec-y87','a_xyz_1sec-z87','a_xyz_1sec-x88','a_xyz_1sec-y88','a_xyz_1sec-z88','a_xyz_1sec-x89','a_xyz_1sec-y89','a_xyz_1sec-z89','a_xyz_1sec-x90','a_xyz_1sec-y90','a_xyz_1sec-z90','a_xyz_1sec-x91','a_xyz_1sec-y91','a_xyz_1sec-z91','a_xyz_1sec-x92','a_xyz_1sec-y92','a_xyz_1sec-z92','a_xyz_1sec-x93','a_xyz_1sec-y93','a_xyz_1sec-z93','a_xyz_1sec-x94','a_xyz_1sec-y94','a_xyz_1sec-z94','a_xyz_1sec-x95','a_xyz_1sec-y95','a_xyz_1sec-z95','a_xyz_1sec-x96','a_xyz_1sec-y96','a_xyz_1sec-z96','a_xyz_1sec-x97','a_xyz_1sec-y97','a_xyz_1sec-z97','a_xyz_1sec-x98','a_xyz_1sec-y98','a_xyz_1sec-z98','a_xyz_1sec-x99','a_xyz_1sec-y99','a_xyz_1sec-z99','a_z_spectrum_mean_1sec-B1','a_z_spectrum_mean_1sec-B1_2','a_z_spectrum_mean_1sec-B1_6','a_z_spectrum_mean_1sec-B2','a_z_spectrum_mean_1sec-B2_5','a_z_spectrum_mean_1sec-B3_1','a_z_spectrum_mean_1sec-B3_9','a_z_spectrum_mean_1sec-B4_9','a_z_spectrum_mean_1sec-B6_2','a_z_spectrum_mean_1sec-B7_8','a_z_spectrum_mean_1sec-B9_8','a_z_spectrum_mean_1sec-B12_4','a_z_spectrum_mean_1sec-B15_6','a_z_spectrum_mean_1sec-B19_7','a_z_spectrum_mean_1sec-B24_8','a_z_spectrum_mean_1sec-B31_3','a_z_spectrum_mean_1sec-B39_4','a_z_spectrum_median_1sec-B1','a_z_spectrum_median_1sec-B1_2','a_z_spectrum_median_1sec-B1_6','a_z_spectrum_median_1sec-B2','a_z_spectrum_median_1sec-B2_5','a_z_spectrum_median_1sec-B3_1','a_z_spectrum_median_1sec-B3_9','a_z_spectrum_median_1sec-B4_9','a_z_spectrum_median_1sec-B6_2','a_z_spectrum_median_1sec-B7_8','a_z_spectrum_median_1sec-B9_8','a_z_spectrum_median_1sec-B12_4','a_z_spectrum_median_1sec-B15_6','a_z_spectrum_median_1sec-B19_7','a_z_spectrum_median_1sec-B24_8','a_z_spectrum_median_1sec-B31_3','a_z_spectrum_median_1sec-B39_4','a_z_spectrum_05_1sec-B1','a_z_spectrum_05_1sec-B1_2','a_z_spectrum_05_1sec-B1_6','a_z_spectrum_05_1sec-B2','a_z_spectrum_05_1sec-B2_5','a_z_spectrum_05_1sec-B3_1','a_z_spectrum_05_1sec-B3_9','a_z_spectrum_05_1sec-B4_9','a_z_spectrum_05_1sec-B6_2','a_z_spectrum_05_1sec-B7_8','a_z_spectrum_05_1sec-B9_8','a_z_spectrum_05_1sec-B12_4','a_z_spectrum_05_1sec-B15_6','a_z_spectrum_05_1sec-B19_7','a_z_spectrum_05_1sec-B24_8','a_z_spectrum_05_1sec-B31_3','a_z_spectrum_05_1sec-B39_4','a_z_spectrum_95_1sec-B1','a_z_spectrum_95_1sec-B1_2','a_z_spectrum_95_1sec-B1_6','a_z_spectrum_95_1sec-B2','a_z_spectrum_95_1sec-B2_5','a_z_spectrum_95_1sec-B3_1','a_z_spectrum_95_1sec-B3_9','a_z_spectrum_95_1sec-B4_9','a_z_spectrum_95_1sec-B6_2','a_z_spectrum_95_1sec-B7_8','a_z_spectrum_95_1sec-B9_8','a_z_spectrum_95_1sec-B12_4','a_z_spectrum_95_1sec-B15_6','a_z_spectrum_95_1sec-B19_7','a_z_spectrum_95_1sec-B24_8','a_z_spectrum_95_1sec-B31_3','a_z_spectrum_95_1sec-B39_4','device_id','revision_code','trip_id','segment_id','edgeid','basenodeid','adjnodeid','lonfrom','latfrom','lonto','latto','order']]
    

## do something with accel_x, accel_y, accel_z

combinedData = pd.DataFrame()

Frames = []
previousTime = None


arr = contents.sort_values(by=['time'])
for index, row in arr.iterrows():

    t = datetime.strptime(row['time'], "%Y-%m-%d %H:%M:%S")
    timerow = np.array([t + timedelta(seconds=0.01*i) for i in range(100)])
    
    frame = pd.DataFrame(index = timerow)
    zeroRow = np.array([0 for i in range(100)])
    frame['gps'] = zeroRow
    frame['lonto'] = zeroRow
    frame ['latto'] = zeroRow
    frame['gpsspeed'] = zeroRow
    
    frame['gps'].iloc[0] = 1 
    frame['lonto'].iloc[0] = row['lonto']
    frame ['latto'].iloc[0] = row['latto']
    frame['gpsspeed'].iloc[0] = row['GPSau-speed']
    
    
    r = row[['a_xyz_1sec-x0','a_xyz_1sec-y0','a_xyz_1sec-z0','a_xyz_1sec-x1','a_xyz_1sec-y1','a_xyz_1sec-z1','a_xyz_1sec-x2','a_xyz_1sec-y2','a_xyz_1sec-z2','a_xyz_1sec-x3','a_xyz_1sec-y3','a_xyz_1sec-z3','a_xyz_1sec-x4','a_xyz_1sec-y4','a_xyz_1sec-z4','a_xyz_1sec-x5','a_xyz_1sec-y5','a_xyz_1sec-z5','a_xyz_1sec-x6','a_xyz_1sec-y6','a_xyz_1sec-z6','a_xyz_1sec-x7','a_xyz_1sec-y7','a_xyz_1sec-z7','a_xyz_1sec-x8','a_xyz_1sec-y8','a_xyz_1sec-z8','a_xyz_1sec-x9','a_xyz_1sec-y9','a_xyz_1sec-z9','a_xyz_1sec-x10','a_xyz_1sec-y10','a_xyz_1sec-z10','a_xyz_1sec-x11','a_xyz_1sec-y11','a_xyz_1sec-z11','a_xyz_1sec-x12','a_xyz_1sec-y12','a_xyz_1sec-z12','a_xyz_1sec-x13','a_xyz_1sec-y13','a_xyz_1sec-z13','a_xyz_1sec-x14','a_xyz_1sec-y14','a_xyz_1sec-z14','a_xyz_1sec-x15','a_xyz_1sec-y15','a_xyz_1sec-z15','a_xyz_1sec-x16','a_xyz_1sec-y16','a_xyz_1sec-z16','a_xyz_1sec-x17','a_xyz_1sec-y17','a_xyz_1sec-z17','a_xyz_1sec-x18','a_xyz_1sec-y18','a_xyz_1sec-z18','a_xyz_1sec-x19','a_xyz_1sec-y19','a_xyz_1sec-z19','a_xyz_1sec-x20','a_xyz_1sec-y20','a_xyz_1sec-z20','a_xyz_1sec-x21','a_xyz_1sec-y21','a_xyz_1sec-z21','a_xyz_1sec-x22','a_xyz_1sec-y22','a_xyz_1sec-z22','a_xyz_1sec-x23','a_xyz_1sec-y23','a_xyz_1sec-z23','a_xyz_1sec-x24','a_xyz_1sec-y24','a_xyz_1sec-z24','a_xyz_1sec-x25','a_xyz_1sec-y25','a_xyz_1sec-z25','a_xyz_1sec-x26','a_xyz_1sec-y26','a_xyz_1sec-z26','a_xyz_1sec-x27','a_xyz_1sec-y27','a_xyz_1sec-z27','a_xyz_1sec-x28','a_xyz_1sec-y28','a_xyz_1sec-z28','a_xyz_1sec-x29','a_xyz_1sec-y29','a_xyz_1sec-z29','a_xyz_1sec-x30','a_xyz_1sec-y30','a_xyz_1sec-z30','a_xyz_1sec-x31','a_xyz_1sec-y31','a_xyz_1sec-z31','a_xyz_1sec-x32','a_xyz_1sec-y32','a_xyz_1sec-z32','a_xyz_1sec-x33','a_xyz_1sec-y33','a_xyz_1sec-z33','a_xyz_1sec-x34','a_xyz_1sec-y34','a_xyz_1sec-z34','a_xyz_1sec-x35','a_xyz_1sec-y35','a_xyz_1sec-z35','a_xyz_1sec-x36','a_xyz_1sec-y36','a_xyz_1sec-z36','a_xyz_1sec-x37','a_xyz_1sec-y37','a_xyz_1sec-z37','a_xyz_1sec-x38','a_xyz_1sec-y38','a_xyz_1sec-z38','a_xyz_1sec-x39','a_xyz_1sec-y39','a_xyz_1sec-z39','a_xyz_1sec-x40','a_xyz_1sec-y40','a_xyz_1sec-z40','a_xyz_1sec-x41','a_xyz_1sec-y41','a_xyz_1sec-z41','a_xyz_1sec-x42','a_xyz_1sec-y42','a_xyz_1sec-z42','a_xyz_1sec-x43','a_xyz_1sec-y43','a_xyz_1sec-z43','a_xyz_1sec-x44','a_xyz_1sec-y44','a_xyz_1sec-z44','a_xyz_1sec-x45','a_xyz_1sec-y45','a_xyz_1sec-z45','a_xyz_1sec-x46','a_xyz_1sec-y46','a_xyz_1sec-z46','a_xyz_1sec-x47','a_xyz_1sec-y47','a_xyz_1sec-z47','a_xyz_1sec-x48','a_xyz_1sec-y48','a_xyz_1sec-z48','a_xyz_1sec-x49','a_xyz_1sec-y49','a_xyz_1sec-z49','a_xyz_1sec-x50','a_xyz_1sec-y50','a_xyz_1sec-z50','a_xyz_1sec-x51','a_xyz_1sec-y51','a_xyz_1sec-z51','a_xyz_1sec-x52','a_xyz_1sec-y52','a_xyz_1sec-z52','a_xyz_1sec-x53','a_xyz_1sec-y53','a_xyz_1sec-z53','a_xyz_1sec-x54','a_xyz_1sec-y54','a_xyz_1sec-z54','a_xyz_1sec-x55','a_xyz_1sec-y55','a_xyz_1sec-z55','a_xyz_1sec-x56','a_xyz_1sec-y56','a_xyz_1sec-z56','a_xyz_1sec-x57','a_xyz_1sec-y57','a_xyz_1sec-z57','a_xyz_1sec-x58','a_xyz_1sec-y58','a_xyz_1sec-z58','a_xyz_1sec-x59','a_xyz_1sec-y59','a_xyz_1sec-z59','a_xyz_1sec-x60','a_xyz_1sec-y60','a_xyz_1sec-z60','a_xyz_1sec-x61','a_xyz_1sec-y61','a_xyz_1sec-z61','a_xyz_1sec-x62','a_xyz_1sec-y62','a_xyz_1sec-z62','a_xyz_1sec-x63','a_xyz_1sec-y63','a_xyz_1sec-z63','a_xyz_1sec-x64','a_xyz_1sec-y64','a_xyz_1sec-z64','a_xyz_1sec-x65','a_xyz_1sec-y65','a_xyz_1sec-z65','a_xyz_1sec-x66','a_xyz_1sec-y66','a_xyz_1sec-z66','a_xyz_1sec-x67','a_xyz_1sec-y67','a_xyz_1sec-z67','a_xyz_1sec-x68','a_xyz_1sec-y68','a_xyz_1sec-z68','a_xyz_1sec-x69','a_xyz_1sec-y69','a_xyz_1sec-z69','a_xyz_1sec-x70','a_xyz_1sec-y70','a_xyz_1sec-z70','a_xyz_1sec-x71','a_xyz_1sec-y71','a_xyz_1sec-z71','a_xyz_1sec-x72','a_xyz_1sec-y72','a_xyz_1sec-z72','a_xyz_1sec-x73','a_xyz_1sec-y73','a_xyz_1sec-z73','a_xyz_1sec-x74','a_xyz_1sec-y74','a_xyz_1sec-z74','a_xyz_1sec-x75','a_xyz_1sec-y75','a_xyz_1sec-z75','a_xyz_1sec-x76','a_xyz_1sec-y76','a_xyz_1sec-z76','a_xyz_1sec-x77','a_xyz_1sec-y77','a_xyz_1sec-z77','a_xyz_1sec-x78','a_xyz_1sec-y78','a_xyz_1sec-z78','a_xyz_1sec-x79','a_xyz_1sec-y79','a_xyz_1sec-z79','a_xyz_1sec-x80','a_xyz_1sec-y80','a_xyz_1sec-z80','a_xyz_1sec-x81','a_xyz_1sec-y81','a_xyz_1sec-z81','a_xyz_1sec-x82','a_xyz_1sec-y82','a_xyz_1sec-z82','a_xyz_1sec-x83','a_xyz_1sec-y83','a_xyz_1sec-z83','a_xyz_1sec-x84','a_xyz_1sec-y84','a_xyz_1sec-z84','a_xyz_1sec-x85','a_xyz_1sec-y85','a_xyz_1sec-z85','a_xyz_1sec-x86','a_xyz_1sec-y86','a_xyz_1sec-z86','a_xyz_1sec-x87','a_xyz_1sec-y87','a_xyz_1sec-z87','a_xyz_1sec-x88','a_xyz_1sec-y88','a_xyz_1sec-z88','a_xyz_1sec-x89','a_xyz_1sec-y89','a_xyz_1sec-z89','a_xyz_1sec-x90','a_xyz_1sec-y90','a_xyz_1sec-z90','a_xyz_1sec-x91','a_xyz_1sec-y91','a_xyz_1sec-z91','a_xyz_1sec-x92','a_xyz_1sec-y92','a_xyz_1sec-z92','a_xyz_1sec-x93','a_xyz_1sec-y93','a_xyz_1sec-z93','a_xyz_1sec-x94','a_xyz_1sec-y94','a_xyz_1sec-z94','a_xyz_1sec-x95','a_xyz_1sec-y95','a_xyz_1sec-z95','a_xyz_1sec-x96','a_xyz_1sec-y96','a_xyz_1sec-z96','a_xyz_1sec-x97','a_xyz_1sec-y97','a_xyz_1sec-z97','a_xyz_1sec-x98','a_xyz_1sec-y98','a_xyz_1sec-z98','a_xyz_1sec-x99','a_xyz_1sec-y99','a_xyz_1sec-z99']]
    x_acc = r.values[0::3].flatten()
    y_acc = r.values[1::3].flatten()  
    z_acc = r.values[2::3].flatten()
    
    frame['ax'] = x_acc
    frame['ay'] = y_acc
    frame['az'] = z_acc
    
    
    if previousTime == None:
        previousTime = t
    
    #print((t - previousTime))
        
    if  (t - previousTime) < timedelta(seconds=3):
        combinedData = pd.concat([ combinedData,frame])
    else:
        combinedData.index = combinedData.index - combinedData.index.values[0]   
        combinedData['seconds'] = combinedData.index
        combinedData['seconds'] = combinedData['seconds'].dt.total_seconds() 
        Frames.append( combinedData )
        combinedData =  frame
    
    previousTime = t
    
combinedData.index = combinedData.index - combinedData.index.values[0]   
combinedData['seconds'] = combinedData.index
combinedData['seconds'] = combinedData['seconds'].dt.total_seconds() 
Frames.append( combinedData )
combinedData =  frame


for i,frame in enumerate(Frames,1):
    #print(frame)
    #print(i)
    #plt.figure( str(i) )
    #plt.plot( frame.index.values, frame['az'])
    #plt.draw()

    frame.to_csv( pathN +'_'+str(i)+'.csv', sep=';')
    

#plt.show()



