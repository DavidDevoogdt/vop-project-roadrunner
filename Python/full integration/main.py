import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal

df = pd.read_table('nulpuntsmeting2019-02-25_03-42-21.txt', delimiter=";", header=0)
#df = pd.read_table('2019-02-25_03-11-33.txt', delimiter=";", header=0)

#____________calibration_______________

#ax             0.121568506568460
#ay            -0.000054966195790
#az           -10.933853359534035
#gx            -2.356726986203252
#gy             0.810309998351138
#gz            -0.311074784257679



df['time'] = df['time'] - df['time'][0]  #set t to zero
df['deltatime'] =  df['time'] - df.shift(periods=1)['time'] #calculate time delta (needed for integration

print(df)

#df = df[ df['time']>=30] #account for impresions mpu in first 30 seconds
#df = df[ df['time']<=35]
fs = 1./(df.mean()["deltatime"])

#print(df)
#pd.set_option('precision', 15)
#print(df.mean())
#pd.set_option('precision', 5)

#adjust for systematic bias (null point reference measurement
df['ax'] = df['ax']- 0.121568506568460
df['ay'] = df['ay']+ 0.000054966195790
df['az'] = df['az']+ 10.933853359534035 - 9.8116
df['wx'] = df['gx']+2.356726986203252
df['wy'] = df['gy']-0.810309998351138
df['wz'] = df['gz']+0.311074784257679



print(df)

#______________GYRO ANGLES and real accel________________________________
# extra values needed for construction orientation matrix
df['wn'] = np.sqrt( df['wx']**2 + df['wy']**2 + df['wz']**2) 
df['wnx'] = df['wx']/df['wn']
df['wny'] = df['wy']/df['wn']
df['wnz'] = df['wz']/df['wn']
df['cwndt'] = np.cos( df['wn']*df['deltatime'])
df['swndt'] = np.sin( df['wn']*df['deltatime'])
# angles estimation using accelero
df['accPitch'] = np.arctan2(df['ax'], (df['ax']**2 + df['az']**2 )** 0.5)
df['accRoll'] = np.arctan2(df['ay'], (df['ax']**2 + df['az']**2 )** 0.5)
df['accYaw'] = np.arctan2(df['az'], (df['ax']**2 + df['az']**2 )** 0.5)


#calculate rotation matrix for each state  ori*[0,0,1] would give the z-axis of the gyro in real world coordinates
#cannot be vectorized because previous rotation matrix is needed before next one can be calculated

ori = np.array( [[1,0,0],
                 [0,1,0],
                 [0,0,1]])
#print(df)

linearParam = 0.02

for i,row in df[1:].iterrows():
    
    row = df.loc[[i]]
    a = float(row['wnx'])
    b = float(row['wny'])
    c = float(row['wnz'])
    ct = float(row['cwndt'])
    st = float(row['swndt'])
    lp = row['accPitch'].values[0]
    lr = row['accRoll'].values[0]
    ly = row['accYaw'].values[0]
               
    R = np.array( [[a**2+(1-a**2)*ct, a*b*(1-ct)-c*st,    a*c*(1-ct)+b*st  ],
                    [a*b*(1-ct)+c*st,  b**2 + (1-b**2)*ct, b*c*(1-ct)-a*st ],
                    [a*c*(1-ct)-b*st, b*c*(1-ct)+a*st,    c**2+(1-c**2)*ct]] )
    ori = np.matmul(np.array(R) , ori)   
    #https://stackoverflow.com/questions/11514063/extract-yaw-pitch-and-roll-from-a-rotationmatrix antwoord2 (z-x-z)
    
    # use mainly param from gyro and correct over time with param accel
    newYaw =  (1-linearParam) * np.arctan2( ori[1,0],ori[0,0] )+ linearParam*ly
    newPitch = (1-linearParam) *np.arctan2(-ori[2,0], np.sqrt(ori[2,1]**2+ori[2,2]**2) )+ linearParam*lp
    newRoll = (1-linearParam) *np.arctan2( ori[2,1],ori[2,2] )+ linearParam*lr
    
    df.loc[[i],'yaw'] = newYaw
    df.loc[[i],'pitch'] = newPitch
    df.loc[[i],'roll'] =  newRoll
    
    c1 = np.cos(newYaw)
    s1 = np.sin(newYaw)
    c2 = np.cos(newPitch)
    s2 = np.sin(newPitch)
    c3 = np.cos(newRoll)
    s3 = np.sin(newRoll)
    
    ori = np.array( [[c1*c3-s1*c2*s3,   -c1*s3 -s1*c2*c,    s1*s2],
                     [s1*c3 + c1*c2*s3, -s1*s3 + c1*c2*c3,  -c1*s2],
                     [s2*s3,            s2*c3,              c2]] )
    
    #print(ori)
    
    ax = row['ax'].values[0]
    ay = row['ay'].values[0]
    az = row['az'].values[0]
    
    ar = np.matmul( ori, np.array([[ax],[ay],[az]]))
    
    df.loc[[i],'arx'] =  ar[0,0]  #arealx
    df.loc[[i],'ary'] =  ar[1,0]
    df.loc[[i],'arz'] =  ar[2,0]
    
print(df)
#___________subtract gravity________________
    
df['arzg'] = df['arz'] + 9.8116 
#___________integrate height profile and distance travelled

df['vx'] = df['arx']*df['deltatime'].cumsum()
df['vy'] = df['ary']*df['deltatime'].cumsum()
df['vz'] = df['arzg']*df['deltatime'].cumsum()
df['px'] = df['vx']*df['deltatime'].cumsum()
df['py'] = df['vy']*df['deltatime'].cumsum()
df['pz'] = df['vz']*df['deltatime'].cumsum()

print(df)

#___________________plotting______________________




plt.figure(1)
plt.plot(df['time'],df['yaw']*180/np.pi,label='yaw')
plt.plot(df['time'],df['roll']*180/np.pi,label='pitch')
plt.plot(df['time'],df['pitch']*180/np.pi,label='roll')
plt.legend()
plt.show()
plt.figure(2)
plt.plot(df['time'],df['vx'],label='vx')
plt.plot(df['time'],df['vy'],label='vy')
plt.plot(df['time'],df['vz'],label='vz')
plt.legend()
plt.show()
plt.figure(3)
plt.plot(df['time'],df['ax'],label='ax')
plt.plot(df['time'],df['ay'],label='ay')
plt.plot(df['time'],df['az'],label='az')
plt.legend()
plt.show()
plt.figure(4)
plt.plot(df['time'],df['arx'],label='arx')
plt.plot(df['time'],df['ary'],label='ary')
plt.plot(df['time'],df['arz'],label='arz')
plt.legend()
plt.show()
plt.figure(5)
plt.plot(df['time'],df['wx'],label='wx')
plt.plot(df['time'],df['wy'],label='wy')
plt.plot(df['time'],df['wz'],label='wz')
plt.legend()
plt.show()
plt.figure(5)
plt.plot(df['time'],df['pz'],label='pz')
plt.legend()
plt.show()
