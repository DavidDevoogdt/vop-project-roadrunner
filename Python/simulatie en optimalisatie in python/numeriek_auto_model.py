from makefunction import *
import numpy as np
kt1=150000
kt2=150000
#kt2=0
k1=66000
k2=18600
#k2 = 0
a1=1.15
a2=-1.05
#a2=0
c1=1190
c2=1100
#c2=0
m=1300
m1=50
m2=50
#m2=50000000
Iz=3440
#Iz=5000000000000000000
F=np.array([[0,0],
            [0,0],
            [kt1, 0],
            [0, kt2]])
K=np.array([[k1+k2,      a2*k2-a1*k1,      -k1,     -k2],
            [a2*k2-a1*k1, k1*a1**2+k2*a2**2, a1*k1, -a2*k2],
            [-k1,         a1*k1,             k1*kt1,  0],
            [-k2,         -a2*k2,            0,       k2+kt2]])
M=np.array([[ m, 0, 0, 0],
            [ 0, Iz, 0, 0],
            [ 0, 0, m1, 0],
            [ 0, 0, 0, m2]])
C=np.array([[ c1+c2,      a2*c2-a1*c1,      -c1,   -c2],
            [a2*c2-a1*c1, c1*a1**2+c2*a2**2, c1*a1, -a2*c2],
            [ -c1,        a1*c1,             c1,     0],
            [ -c2,        -a2*c2,             0,    c2]])


y1=0.3
y2=0.3
M1 =np.linalg.inv(M)
def u1(t):
    return f_rdrempel(t)    

def u(t):
    return 0.01*np.sin(t)
y0=[0, 0, 0, 0, 0, 0, 0, 0]
def Var(y, t):
    [x, theta, x1, x2, v, omega, v1, v2] = y
    dydt = [v, omega, v1, v2,
            -1/m*(c1*(v-v1-a1*omega)+c2*(v-v2-a2*omega)+k1*(x-x1-a1*theta)+k2*(x-x2-a2*theta)),
            -1/Iz*(-a1*c1*(v-v1-a1*omega)-a2*c2*(v-v2-a2*omega)-a1*k1*(x-x1-a1*theta)-a2*k2*(x-x2-a2*theta)),
            -1/m1*(-c1*(v-v1-a1*omega)+kt1*(x1-u1(t+a1/speed))-k1*(x-x1-a1*theta)),
            -1/m2*(-c2*(v-v2-a2*omega)+kt2*(x2-u1(t+a2/speed))-k2*(x-x2-a2*theta))]
    return dydt
#def Var(y,t):
#    [x,theta,x1,x2,v,omega,v1,v2]=y
#    X = np.array([x,theta,x1,x2])
#    XX = np.array([v,omega,v1,v2])
#    U = np.array([u1(t+a1/speed),u1(t-a2/speed)])
#    
#    [xxx, thetaxx, x1xx, x2xx] = -M1.dot(C.dot(XX)+K.dot(X)-F.dot(U))
#    dydt = [v, omega, v1, v2, xxx, thetaxx, x1xx, x2xx]
#    return dydt
#def Var(y, t):
#    [x, theta, x1, x2, v, omega, v1, v2] = y
#    dydt = [v, omega, v1, v2,
#            -1/m*(c1*(v-v1+a1*omega)+c2*(v-v2-a2*omega)+k1*(x-x1+a1*theta)+k2*(x-x2-a2*theta)),
#            -1/Iz*(a1*c1*(v-v1+a1*omega)-a2*c2*(v-v2-a2*omega)+a1*k1*(x-x1+a1*theta)-a2*k2*(x-x2-a2*theta)),
#            -1/m1*(-c1*(v-v1+a1*omega)+kt1*(x1-u1(t))-k1*(x-x1+a1*theta)),
#            -1/m2*(-c2*(v-v2-a2*omega)+kt2*(x2-u1(t-(a1+a2)/8.8))-k2*(x-x2-a2*theta))]
#    return dydt
y0=[0, 0, 0, 0, 0 , 0, 0, 0]
t = np.linspace(-0.05, 2, 10000)
from scipy.integrate import odeint
sol = odeint(Var, y0, t)
import matplotlib.pyplot as plt
plt.plot(t, sol[:, 0], 'b', label='X(t)')
plt.plot(t, sol[:, 1], 'g', label='theta(t)')
#
plt.plot(t, sol[:, 2], 'y', label='x1(t)')
plt.plot(t, sol[:, 3], label='x2(t)')
plt.plot(t, u1(t),'r', label='y(t)')
plt.legend(loc='best')
plt.xlabel('t')
plt.grid()
plt.show()