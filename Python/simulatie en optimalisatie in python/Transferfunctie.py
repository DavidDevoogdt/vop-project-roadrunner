# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 02:09:55 2019

@author: rbnwy
"""
from QuarterCar import *
from random import *
from collections import *
plt.clf()

##
## To do: maken dat je een ander signaal gebruikt of later pas kijkt naar resultaat want momenteel heb je evenwichtstoestand die niet constant is 
##


param=OrderedDict()
param['vx']=5
param['k_downforce']=0*9.81/5**2 #0.3g (?)
param["lengte"]=2.5
param["afstandautowiel"]=0.8
param["straal_wiel"]=0.2 
#param["contact_length"]=0.2*40/100 #20% vd diamter
param["contact_length"]=0 #20% vd diamter
param["m_a"]=500
param["m_w"]=50
param["k_aw"]=60000
param["k_wg"]=300000
param["c"]=30000
param["I"]=0
param["l0_autowiel"]=0.8

#verdere specificaties
#signaal=lambda x: 0.02*math.sin(x*2*math.pi*10) 
def nieuwe_sin(x):
    if x < 5:
        return 0
    else:
        return A*math.sin(x*2*math.pi*f)
signaal=sin #MOET AANGEPASTE SIN WORDEN
algoritme=RK4 #RK4,Euler (is 2 à 3 keer sneller maar minder nauwkeurig bij lage fps)
met_shift=False #met shift is het achterwiel eerst vlak; False is vooral interessant bij sin functie zodat de wielen precies hetzelfde doen
allow_flying=False #als de banden niet ingedrukt worden, creëren ze geen kracht
fps=300
tot_tijd=10
metExecutietijd=False

#resonantie

param['vx']=1
s=Simulation(*list(param.values()),algoritme,signaal,met_shift,allow_flying)
s.f=100
s.simulate(fps,tot_tijd,metExecutietijd)
#s.output("hoogtePlot")
#s.output("Gui")
print((s.output("hoogteLijst").max()-s.output("hoogteLijst").min())/0.2)

xas=[]
Tffie=[]
simuls = []
for i in np.geomspace(10**(-6),10**10,num=30):
    param['vx']=math.sqrt(i)
    s=Simulation(*list(param.values()),algoritme,signaal,met_shift,allow_flying)
    s.f=math.sqrt(i)
    s.wielfreq=i
    s.simulate(fps,tot_tijd,metExecutietijd)
    xas.append(s.wielfreq)
    failed=False
    while s.state != "Regular":
        if fps < 10000:
            print("Verdubbeling van fps en tot_tijd: %s" % (fps))
            fps*=2
            tot_tijd*=2
            s.simulate(fps,tot_tijd,metExecutietijd)
        else:
            print("Te hoge fps, aborting")
            fps=300
            tot_tijd=2
            failed=True
            #s.output("Gui")
            break
    if failed:
        Tffie.append(None)
    else:
        Tffie.append((s.output("hoogteLijst").max()-s.output("hoogteLijst").min())/0.2)
        simuls.append(s)
plt.loglog(np.array(xas),np.array(Tffie))    

