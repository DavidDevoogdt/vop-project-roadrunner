# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 11:06:19 2019

@author: rbnwy
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 02:09:55 2019

@author: rbnwy
"""
from QuarterCar import *
from random import *
from collections import *
from operator import itemgetter
import seaborn as sns
import numpy as np 
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 


plt.close("all")

#specificaties
signaal=trapezium #vlak,ruis,step,trapezium,sin
algoritme=Euler #RK4,Euler (is 2 à 3 keer sneller maar minder nauwkeurig bij lage fps)
met_shift=False #zonder shift is het achterwiel eerst vlak; vooral interessant bij sin functie zodat de wielen precies hetzelfde doen
allow_flying=False #als de banden niet ingedrukt worden, creëren ze geen kracht
fps=300
tot_tijd=2
metExecutietijd=False

def printDict(d):
    out=''
    for i,(key,value) in enumerate(init.items()):
        s=key+': '+str(value)+'\n'
        out+=s
    return out

#te reconstrueren parameters
doel=OrderedDict()
doel['vx']=5
doel['k_downforce']=0.3*9.81/5**2 #0.3g (?)
doel["lengte"]=2.5
doel["afstandautowiel"]=0.8
doel["straal_wiel"]=0.2 
doel["contact_length"]=0.2*40/100 #20% vd diamter
doel["m_a"]=500
doel["m_w"]=50
doel["k_aw"]=60000
doel["k_wg"]=300000
doel["c"]=3000
doel["I"]=3000
doel["l0_autowiel"]=0.8

init=OrderedDict()
init['vx']=5
init['k_downforce']=0.3*9.81/5**2 #0.3g (?)
init["lengte"]=2.5
init["afstandautowiel"]=0.8
init["straal_wiel"]=0.2 
init["contact_length"]=0.3*40/100 #20% vd diamter
init["m_a"]=500
init["m_w"]=50
init["k_aw"]=60000
init["k_wg"]=300000
init["c"]=3000
init["I"]=3000
init["l0_autowiel"]=0.8

for i,(key,value) in enumerate(init.items()):
    init[key]=value*(random()+0.5)


doelSimul=Simulation(*list(doel.values()),algoritme,signaal,met_shift,allow_flying)
doelSimul.simulate(fps,tot_tijd,metExecutietijd)
doelOut = doelSimul.output("hoogteLijst")

initSimul=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
initSimul.simulate(fps,tot_tijd,metExecutietijd)
initOut = initSimul.output("hoogteLijst")


#doelSimul.output("hoogtePlot")
#initSimul.output("hoogtePlot")

def mse(s1,s2):
    return ((s1.output("hoogteLijst")-s2.output("hoogteLijst"))**2).mean()

def hyperSweep():
    #ax1 = fig.add_subplot(111, projection='3d')
    data=np.zeros((13,10))
    animationData = []
    hyperSweep = []
    for i,(key,value) in enumerate(init.items()):
        singleSweep = []
        for j,v in enumerate(np.linspace(0.5,1.5,10)):
            init[key]=value*v
            s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
            s.simulate(fps,tot_tijd,metExecutietijd)
            if s.state == "Regular":
                singleSweep.append((v,mse(s,doelSimul)))
                data[i,j]=-math.log(mse(s,doelSimul))
            else:
                singleSweep.append((v,math.inf))
                data[i,j]=None
            init[key]=value    
            
            #animationData.append(np.copy(data))
            #animate
            #sns.heatmap(animationData[-1],cbar=False)            
            #3d bar plot is traag:
            #_x = np.arange(1,14)
            #_y = np.arange(1,11)
            #_xx, _yy = np.meshgrid(_x, _y)
            #x, y = _xx.ravel(), _yy.ravel()
            #z = animationData[-1].ravel()
            #ax1.bar3d(x,y,np.zeros_like(z),1,1,z,shade=True,color='skyblue')
            #plt.pause(.0001)
        hyperSweep.append((key,value*min(singleSweep,key=itemgetter(1))[0],min(singleSweep,key=itemgetter(1))[1]))
    result = min(hyperSweep,key=itemgetter(2))
    init[result[0]]=result[1]
    print(result)
    return data



print(mse(doelSimul,initSimul))
plt.ion()
i=0
while True:
    i+=1
    if i == 1 or i == 2:
        data = hyperSweep()
        plt.figure(i,figsize=(10,10))
        sns.heatmap(data,xticklabels=np.linspace(0.5,1.5,10).round(decimals=2),yticklabels=init.keys(),annot=True,cbar=True)
        plt.title(printDict(init))
        plt.subplots_adjust(top=0.6)
        plt.pause(.5) #tijd die je hebt om het scherm te verslepen nadat het gemaakt wordt. Daarna bevriest het en gaat de bereking door
        plt.show()
    else:
        plt.close(plt.figure(i-2))
        data = hyperSweep()
        plt.figure(i,figsize=(10,10))
        sns.heatmap(data,xticklabels=np.linspace(0.5,1.5,10).round(decimals=2),yticklabels=init.keys(),annot=True,cbar=True)
        plt.title(printDict(init))
        plt.subplots_adjust(top=0.6)
        plt.pause(.5)
        plt.show()
    initSimul=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
    initSimul.simulate(fps,tot_tijd,metExecutietijd)
    print(mse(doelSimul,initSimul))



