# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 16:22:09 2019

@author: robin
"""
import csv
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
from datetime import datetime, timedelta


def runningAvg(series, N):
    padded = np.pad(series, (N // 2, N - 1 - N // 2), mode='edge')
    return np.convolve(padded, np.ones((N,)) / N, mode='valid')



df = pd.read_csv('readings/A_4.csv', delimiter=";", header=0, index_col =0 )


cutoff = 20
freq = 100
b, a = signal.butter(2, (cutoff / freq), btype='low', analog=False)

df['az'] = signal.filtfilt(b, a, df['az'])




t2 = df[ df['az'] > 1.6].iloc[0]['seconds']



speed =  df[ df['seconds'] == np.ceil(t2)].iloc[0]['gpsspeed']
x1=[]
y1=[]

with open('justprofiel_bumper.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        y1.append(float(row[3])/1000)
x1 = np.array([0.1*i for i in range(len(y1))])
y1 = np.array(y1)
f_gdrempel=  interp1d(x1, y1, kind='cubic')
x=[]
y=[]
with open('rode bumper profiel.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        try:
            y.append(-float(row[2].replace(',','.')))
        except:
            0
x = np.array([0.1*i/speed for i in range(-500,len(y)+500)])
y = np.concatenate((np.zeros(500),np.array(y),np.zeros(500)),axis=None)
f_rdrempel = interp1d(x, y, kind='cubic')