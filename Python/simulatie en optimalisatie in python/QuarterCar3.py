from tkinter import *
import time
import math
import numpy as np
import matplotlib.pyplot as plt

class Point():
    def __init__(self,x,y,m,r,color):
        #eigenschappen
        self.m = m       
        self.r = r
        self.color=color
        #dynamica
        self.x = x
        self.y = y
        self.vx=0
        self.vy=0
class SolidPoint():
    def __init__(self,x0,y0,contact_length,fcn_x,fcn_y):
        self.r=0.02
        self.contact_length=contact_length
        self.x=x0
        self.y=y0
        self.x0=x0
        self.y0=y0
        self.fcn_x=fcn_x
        self.fcn_y=fcn_y
        self.lastpoint_x=self.x0
        self.lastpoint_y=self.y0
        
    def update(self):
        self.x+=snelheid(t)*dt
        self.y=self.y0+self.fcn_y(self.x-self.x0)
        
    def getHeigth(self,tijd):
        self.y1=self.y0+self.fcn_y(self.x-self.x0-self.contact_length/2)
        self.y2=self.y0+self.fcn_y(self.x-self.x0+self.contact_length/2)        
        return (self.y1+self.y2)/2
class QuarterTyre():
    def __init__(self, x0, wegdek, algoritme, shift, p, parentSimulation):
        #shift in seconden
        self.wegdek=lambda x: wegdek(x-shift) if x > shift else 0
        self.algoritme=algoritme
        self.x0=x0
        self.parentSimulation=parentSimulation
        
# =============================================================================
#         self.afstandautowiel=0.8
#         self.straal_wiel=0.2
#         self.contact_length=self.straal_wiel/2.5
#         self.m_a=500
#         self.m_w=50
#         self.k_aw=60000
#         self.k_wg=300000
#         self.c=3000
#         self.I=3000
# =============================================================================
        
        self.afstandautowiel=p[0]
        self.straal_wiel=p[1]
        self.contact_length=p[2]
        self.m_a=p[3]
        self.m_w=p[4]
        self.k_aw=p[5]
        self.k_wg=p[6]
        self.c=p[7]
        self.I=p[8]

        
        self.auto = Point(self.x0,-self.afstandautowiel-self.straal_wiel,self.m_a,0.01,"black")
        self.wiel = Point(self.x0,-self.straal_wiel,self.m_w,0.2,"black")
        self.grond = SolidPoint(self.x0,0,self.contact_length,lambda x: x,self.wegdek) 
        
        self.l0_1=p[9]-((self.m_a+self.m_w)*g+downforce)*(self.k_aw+self.k_wg)/(self.k_aw*self.k_wg)
        self.l0_2=self.straal_wiel
        self.auto.vx=snelheid(0.01)
        self.wiel.vx=snelheid(0.01)
    def update(self):
        w=np.array([self.auto.y,self.auto.vy,self.wiel.y,self.wiel.vy])
        z=self.algoritme(self.f,t,w)
        self.auto.y  = z[0]
        self.auto.vy = z[1]
        self.wiel.y  = z[2]
        self.wiel.vy = z[3]
        #staat omgekeerd!
        if z[0] > self.grond.getHeigth(t)+1 or z[2] > self.grond.getHeigth(t)+1:
            self.parentSimulation.state="Door grond gezakt. Probeer fps te verhogen?"
        elif z[0] < -3 or z[2] < -3:
            self.parentSimulation.state="Vliegende auto"
        else:
            self.parentSimulation.hoogte[frame]=-z[0]
            self.parentSimulation.acc[frame]=-self.f(t,w)[1]
        
        self.auto.x+=snelheid(t)*dt
        self.wiel.x+=snelheid(t)*dt
        self.grond.update()

    def a(self,tijd):
        spring_aw = self.k_aw*(abs(self.auto.y-self.wiel.y)-self.l0_1)
        spring_wg = self.k_wg*(abs(self.wiel.y-self.grond.getHeigth(tijd))-self.l0_2)
        #er moet contact zijn 
        if spring_wg > 0 and allow_flying:
            spring_wg=0
        damper_aw = self.c*(self.auto.vy-self.wiel.vy)
        return (spring_aw - damper_aw)/self.m_a
    def f(self, tijd, w):
        #diff vgl dw/dt=f(t,w)

        #w=[y_a,vy_a,y_w,vy_w]
        #z=[v_a,ay_a,v_w,ay_w]
        
        spring_aw = self.k_aw*(abs(w[0]-w[2])-self.l0_1)
        spring_wg = self.k_wg*(abs(w[2]-self.grond.getHeigth(tijd))-self.l0_2)
        #er moet contact zijn 
        if spring_wg > 0 and allow_flying:
            spring_wg=0
        damper_aw = self.c*(w[1]-w[3])
        
        my_accel = (spring_aw - damper_aw)/self.m_a
              
        #procentuele fout door 1ste orde benadering
        #print((math.atan((self.otherWheel.auto.y-w[0])/b)-(((self.otherWheel.auto.y-w[0])/b)))/(math.atan((self.otherWheel.auto.y-w[0])/b)*2)*100)
        
        h2=self.otherWheel.auto.y
        h1=w[0]
        b = abs(self.otherWheel.x0-self.x0)
        cosatan=math.cos(math.atan((h2-h1)/b))
        ding=h1**2-2*h1*h2+h2**2+b**2
        koppel=-self.I/ding**2*(b*ding*(my_accel-self.otherWheel.a(tijd))-2*(w[1]-self.otherWheel.auto.vy)**2*(h1-h2))*cosatan        
        #koppel=0
        
        z=np.zeros(4)
        z[0]=w[1]
        z[2]=w[3]

        z[1] = (spring_aw - damper_aw - koppel)/self.m_a - g - downforce
        z[3] = (-spring_aw + spring_wg + damper_aw)/self.m_w - g - downforce

        return z
def RK4(f, tijd, w):
    #return w + f(tijd, w)*dt        
    s1 = f(tijd, w)
    s2 = f(tijd + dt/2.0, w + dt*s1/2.0)
    s3 = f(tijd + dt/2.0, w + dt*s2/2.0)
    s4 = f(tijd + dt, w + dt*s3)
    
    return w + dt/6.0*(s1 + 2.0*s2 + 2.0*s3 + s4)
def Euler(f, tijd, w):
    return w + f(tijd,w)*dt

#signalen
sin  = lambda x: A*math.sin(x*2*math.pi*f)
ruis = lambda x: A*math.sin(x*1.1*2*math.pi)*math.cos(x*1.7*2*math.pi)*math.sin(x*2.7*2*math.pi)
vlak = lambda x: 0
def step(x):
    if x >= 0.5:
        return -0.1
    else:
        return 0
def trapezium(x):
    A=0.5
    xh=1
    xr=3
    if x<xh:
        return -x*A/xh #y-richting staat omgekeerd...
    elif x>=xh and x<=xh+xr:
        return -A
    elif x>xh+xr and x<2*xh+xr:
        return (x-xh-xr)*A/xh-A
    else:
        return 0

class Simulation():
    def __init__(self,k_downforce,_lengte,afstandautowiel,straal_wiel,contact_length,m_a,m_w,k_aw,k_wg,c,I,l0_autowiel,algoritme,signaal,_snelheid,met_shift,_allow_flying):
        global g,vx,snelheid,lengte,A,f,downforce,allow_flying
        vx=5
        snelheid=_snelheid
        g=-9.81 #staat omgekeerd
        self.k_downforce = k_downforce
        downforce = -self.k_downforce*vx**2 #staat omgekeerd
        lengte = _lengte
        self.algoritme=algoritme
        self.signaal=signaal
        self.met_shift=met_shift
        allow_flying=_allow_flying
        A=0.2
        self.f=1 #m^(-1), in plaats
        self.wielfreq=vx*self.f
        #signaal=trapezium
        if self.signaal == sin:
            print("%s Hz gevoeld door een wiel" % (self.wielfreq)) #Hz, in tijd, gevoeld door een wiel
            self.signaal = lambda x: A*math.sin(x*2*math.pi*self.f)
        #met_shift=False
        self.p=[afstandautowiel,straal_wiel,contact_length,m_a,m_w,k_aw,k_wg,c,I,l0_autowiel]
        achter = QuarterTyre(0,      self.signaal, self.algoritme, lengte*self.met_shift, self.p, self)
        voor   = QuarterTyre(lengte, self.signaal, self.algoritme, 0,                self.p, self)
        achter.otherWheel=voor
        voor.otherWheel=achter
        self.wagen = [achter,voor]
        self.state="Regular"
    def simulate(self,fps,tot_tijd,metExecutietijd):
        global t,dt,frame,hoogte,acc
        self.fps=fps
        self.tot_tijd=tot_tijd
        self.metExecutietijd=metExecutietijd
        t=0
        dt=1/self.fps
        start_time=time.time()
        aantal=int(self.fps*self.tot_tijd)
        self.tijd=np.linspace(0,self.tot_tijd,aantal)
        self.hoogte=np.zeros(aantal)
        self.acc=np.zeros(aantal)
        for i in range(aantal):
            t=i*dt
            frame=i
            if self.state=="Regular":
                for k in self.wagen:
                    k.update()
            elif metExecutietijd:
                print("Simulation failed at %d s due to: \n %s" % (t,self.state))
                break
            else:
                break
        if self.metExecutietijd:
            print("Executietijd: %s ms" % (1000*(time.time()-start_time)))
    def output(self,what):
        if what == "hoogtePlot":
            plt.plot(self.tijd,self.hoogte)        
        if what == "accPlot":
            plt.plot(self.tijd,self.acc)      
        if what == "hoogteLijst":
            return self.hoogte
        if what == "accLijst":
            return self.acc
        if what == "Gui":
            import Gui3
            Gui3.setInputs(self.k_downforce, lengte, self.p, self.algoritme, self.signaal, snelheid, self.met_shift, allow_flying, self.fps,self.tot_tijd)
            