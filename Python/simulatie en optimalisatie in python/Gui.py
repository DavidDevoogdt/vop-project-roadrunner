# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 10:41:19 2019

@author: rbnwy
"""

from tkinter import *
import time
import math
import numpy as np

class Point():
    def __init__(self,x,y,m,r,color):
        #eigenschappen
        self.m = m       
        self.r = r
        self.color=color
        #dynamica
        self.x = x
        self.y = y
        self.vx=0
        self.vy=0

        self.lastpoint_x=self.x
        self.lastpoint_y=self.y
        self.trace=[]
    def update(self):
        #trace
        stukje = line(self.lastpoint_x,self.lastpoint_y,self.x,self.y,"stippelijn","red")
        self.trace.append((stukje,t))
        self.lastpoint_x=self.x
        self.lastpoint_y=self.y
class SolidPoint():
    def __init__(self,x0,y0,contact_length, fcn_x,fcn_y):
        self.r=0.02
        self.contact_length=contact_length
        self.x=x0
        self.y=y0
        self.x0=x0
        self.y0=y0
        self.fcn_x=fcn_x
        self.fcn_y=fcn_y
        self.lastpoint_x=self.x0
        self.lastpoint_y=self.y0
        
        self.trace = []
    def update(self):
        self.x=self.x0+self.fcn_x(vx*t)
        self.y=self.y0+self.fcn_y(vx*t)
        
        
        #trace
        stukje = line(self.lastpoint_x,self.lastpoint_y,self.x,self.y,"stippelijn","black")
        self.trace.append((stukje,t))
        self.lastpoint_x=self.x
        self.lastpoint_y=self.y
    def getHeigth(self,tijd):
        y1=self.y0+self.fcn_y(vx*tijd-self.contact_length/2)
        y2=self.y0+self.fcn_y(vx*tijd+self.contact_length/2)        
        return (y1+y2)/2
class QuarterTyre():
    def __init__(self, x0, wegdek, shift):
        #shift in seconden
        self.wegdek=lambda x: wegdek(x-shift) if x > shift else 0
        self.x0=x0
        
# =============================================================================
#         self.afstandautowiel=0.8
#         self.straal_wiel=0.2 
#         self.contact_length=self.straal_wiel/2.5
#         self.m_a=500
#         self.m_w=50
#         self.k_aw=60000
#         self.k_wg=300000
#         self.c=3000
#         self.I=3000
# =============================================================================
        
        self.afstandautowiel=p[0]
        self.straal_wiel=p[1]
        self.contact_length=p[2]
        self.m_a=p[3]
        self.m_w=p[4]
        self.k_aw=p[5]
        self.k_wg=p[6]
        self.c=p[7]
        self.I=p[8]
        
        self.auto = Point(self.x0,canvas_heigth/2-self.afstandautowiel-self.straal_wiel,self.m_a,0.01,"black")
        self.wiel = Point(self.x0,canvas_heigth/2-self.straal_wiel,self.m_w,self.straal_wiel,"black")
        self.grond = SolidPoint(self.x0,canvas_heigth/2,self.contact_length,lambda x: x,self.wegdek) 
        
        self.l0_1=p[9]-((self.m_a+self.m_w)*g+downforce)*(self.k_aw+self.k_wg)/(self.k_aw*self.k_wg)
        self.l0_2=self.straal_wiel
        self.auto.vx=vx
        self.wiel.vx=vx
        
        self.shapes = []
    def update(self):
        w=np.array([self.auto.y,self.auto.vy,self.wiel.y,self.wiel.vy])
        z=algoritme(self.f,t,w)
        self.auto.y  = z[0]
        self.auto.vy = z[1]
        self.wiel.y  = z[2]
        self.wiel.vy = z[3]
        #print(self.auto.y)
        
        self.auto.x+=vx*dt
        self.wiel.x+=vx*dt
        self.grond.update()
        if auto_trace==True:
            self.auto.update()
        #eerst alle vormen wegdoen (niet de trace) en dan weer terug tekenen
        for v in self.shapes:
            c.delete(v)
        
        wielStraalVert_onder=self.grond.getHeigth(t)-self.wiel.y
        #er moet contact zijn. Hier wordt de veerkracht ook nul
        if wielStraalVert_onder > self.wiel.r and allow_flying:
            wielStraalVert_onder = self.wiel.r
        wielStraalVert_boven=(0.25*wielStraalVert_onder+1.75*self.wiel.r)/2 #idk waarom ik dit doe ma ziet er niet raar uit
        self.shapes=[
                oval(self.auto.x-self.auto.r, self.auto.y-self.auto.r, self.auto.x+self.auto.r, self.auto.y+self.auto.r,self.auto.color),
                oval(self.wiel.x-self.wiel.r, self.wiel.y-wielStraalVert_boven, self.wiel.x+self.wiel.r, self.wiel.y+wielStraalVert_onder,self.wiel.color),
                oval(self.wiel.x-self.wiel.r/2, self.wiel.y-wielStraalVert_boven/2, self.wiel.x+self.wiel.r/2, self.wiel.y+wielStraalVert_onder/2,"grey"),
                #oval(self.grond.x-self.grond.r, self.grond.y-self.grond.r, self.grond.x+self.grond.r, self.grond.y+self.grond.r,"black"),                                
                
                rectangle(self.auto.x-0.05,self.auto.y,self.wiel.x+0.05,self.wiel.y,"blue"),
                line(self.wiel.x,self.wiel.y,self.auto.x,self.auto.y,"vol","green"),
                line(self.wiel.x,self.wiel.y,self.grond.x,self.wiel.y+wielStraalVert_onder,"vol","red")
                ]
        #contact lengte
        if wielStraalVert_onder < self.wiel.r:
            self.shapes.append(
                line(self.grond.x0+self.grond.fcn_x(vx*t-self.grond.contact_length/2),
                     self.grond.y0+self.grond.fcn_y(vx*t-self.grond.contact_length/2),
                     self.grond.x0+self.grond.fcn_x(vx*t+self.grond.contact_length/2),
                     self.grond.y0+self.grond.fcn_y(vx*t+self.grond.contact_length/2),"vol","yellow"))

    def a(self,tijd):
        spring_aw = self.k_aw*(abs(self.auto.y-self.wiel.y)-self.l0_1)
        spring_wg = self.k_wg*(abs(self.wiel.y-self.grond.getHeigth(tijd))-self.l0_2)
        #er moet contact zijn 
        if spring_wg > 0 and allow_flying:
            spring_wg=0
        damper_aw = self.c*(self.auto.vy-self.wiel.vy)
        return (spring_aw - damper_aw)/self.m_a
    def f(self, tijd, w):
        #diff vgl dw/dt=f(t,w)
        #w=[y_a,vy_a,y_w,vy_w]
        #z=[v_a,ay_a,v_w,ay_w]=f(t,w)
        
        spring_aw = self.k_aw*(abs(w[0]-w[2])-self.l0_1)
        spring_wg = self.k_wg*(abs(w[2]-self.grond.getHeigth(tijd))-self.l0_2)
        #er moet contact zijn 
        if spring_wg > 0 and allow_flying:
            spring_wg=0
        damper_aw = self.c*(w[1]-w[3])
        
        my_accel = (spring_aw - damper_aw)/self.m_a #merk dat ik de zwaartekracht en de downforce er niet bijzet want het is acc_voor-acc_achter die beiden g en downforce al hebben
              
        #procentuele fout door 1ste orde benadering
        #print((math.atan((self.otherWheel.auto.y-w[0])/b)-(((self.otherWheel.auto.y-w[0])/b)))/(math.atan((self.otherWheel.auto.y-w[0])/b)*2)*100)
        
        h2=self.otherWheel.auto.y
        h1=w[0]
        b = abs(self.otherWheel.x0-self.x0)
        cosatan=math.cos(math.atan((h2-h1)/b))
        ding=h1**2-2*h1*h2+h2**2+b**2
        koppel=-self.I/ding**2*(b*ding*(my_accel-self.otherWheel.a(tijd))-2*(w[1]-self.otherWheel.auto.vy)**2*(h1-h2))*cosatan        
        
        z=np.zeros(4)
        z[0]=w[1]
        z[2]=w[3]
        
        z[1] = (spring_aw - damper_aw - koppel)/self.m_a - g - downforce
        z[3] = (-spring_aw + spring_wg + damper_aw)/self.m_w -g -downforce

        return z
def RK4(f, tijd, w):
    #return w + f(tijd, w)*dt        
    s1 = f(tijd, w)
    s2 = f(tijd + dt/2.0, w + dt*s1/2.0)
    s3 = f(tijd + dt/2.0, w + dt*s2/2.0)
    s4 = f(tijd + dt, w + dt*s3)
    
    return w + dt/6.0*(s1 + 2.0*s2 + 2.0*s3 + s4)
def Euler(f, tijd, w):
    return w + f(tijd,w)*dt

#signalen

#signalen
sin  = lambda x: A*math.sin(x*2*math.pi*f)
ruis = lambda x: A*math.sin(x*1.1*2*math.pi)*math.cos(x*1.7*2*math.pi)*math.sin(x*2.7*2*math.pi)
vlak = lambda x: 0
def step(x):
    if x >= 0.5:
        return -0.1
    else:
        return 0
def trapezium(x):
    A=0.5
    xh=1
    xr=3
    if x<xh:
        return -x*A/xh #y-richting staat omgekeerd...
    elif x>=xh and x<=xh+xr:
        return -A
    elif x>xh+xr and x<2*xh+xr:
        return (x-xh-xr)*A/xh-A
    else:
        return 0

#start simulatie

def setInputs(v,k_downforce,_lengte, _p , _algoritme, _signaal, _met_shift, _allow_flying, _fps, _tot_tijd):
    global t,vx, downforce, lengte, p , algoritme, signaal, met_shift, allow_flying, fps, tot_tijd
    vx=v
    downforce = -k_downforce*vx**2 #staat omgekeerd
    lengte=_lengte
    p=_p
    algoritme=_algoritme
    signaal=_signaal
    met_shift=_met_shift
    allow_flying=_allow_flying
    tot_tijd=_tot_tijd
    fps=_fps
    t=0
    Finished=False
    
    #scherm kruisje duwen stopt programma
    while gui.state() == 'normal' and not Finished:
        gui.mainloop()
    start(signaal)
    
def start(nieuw_signaal):    
    global c,t,tijd_text,fps,dt,g,lengte,alpha,A,f,auto_trace, Finished
    try: 
        c.destroy()
    except:
        foo=1
    c = Canvas(gui ,width=alpha*canvas_width,height=alpha*canvas_heigth)
    c.pack()
    
    t=0
    Finished=False
    tijd_text=None
    dt=1/fps
    alpha=scale.get()
    A=0.2
    f=1
    g=-9.81 #staat omgekeerd

    #wagen bouwen
    #A=0.2
    #f=1 #m^(-1), in plaats
    if signaal == sin:
        print("%s Hz gevoeld door een wiel" % (vx*f)) #Hz, in tijd, gevoeld door een wiel
    auto_trace=True
    achter=QuarterTyre(0.5,signaal,lengte*met_shift)
    voor=QuarterTyre(0.5+lengte,signaal,0)
    achter.otherWheel=voor
    voor.otherWheel=achter
    wagen = [achter,voor]
    verbinding = line(wagen[0].auto.x,wagen[0].auto.y,wagen[1].auto.x,wagen[1].auto.y,"vol","black")
    #assenstelsel
    c.create_line(0.5*alpha, (canvas_heigth/2+1.5)*alpha, 1.5*alpha,(canvas_heigth/2+1.5)*alpha-0.5,dash=(),fill="black",arrow=LAST)
    c.create_line(0.5*alpha, (canvas_heigth/2+1.5)*alpha, 0.5*alpha,(canvas_heigth/2+0.5)*alpha-0.5,dash=(),fill="black",arrow=LAST)

    while not Finished:
        t+=dt
        c.delete(tijd_text)
        tijd_text = c.create_text(2*alpha,1*alpha,text="t={0:.{1}f} s".format(t,4)) #user seconde =/= simulatie seconde
        
        for k in wagen:
            k.update()
            c.delete(verbinding)
            verbinding = line(wagen[0].auto.x,wagen[0].auto.y,wagen[1].auto.x,wagen[1].auto.y,"vol","black")
            gui.update()
            time.sleep(dt) #in seconden als het niet lagt
        if t > tot_tijd:
            print("GUI Simulation finished after prescribed %f s" % tot_tijd)
            Finished = True
            
#initializeren   
alpha = 150 #1 meter in SI is zoveel pixels
canvas_width=16
canvas_heigth=8
gui = Tk()
gui.geometry("{}x{}".format(canvas_width*alpha,canvas_heigth*alpha))
gui.title("VOP Runge-Kutta 4de orde")


def oval(x1,y1,x2,y2,kleur):
    return c.create_oval(alpha*x1,alpha*y1,alpha*x2,alpha*y2,fill=kleur)
def rectangle(x1,y1,x2,y2,kleur):
    return c.create_rectangle(alpha*x1,alpha*y1,alpha*x2,alpha*y2,outline=kleur,width=3)
def line(x1,y1,x2,y2,soort,kleur):
    if soort == "stippelijn":    
        return c.create_line(alpha*x1,alpha*y1,alpha*x2,alpha*y2,dash=(2, 4),fill=kleur,width=1)
    if soort == "vol":
        return c.create_line(alpha*x1,alpha*y1,alpha*x2,alpha*y2,dash=(),fill=kleur,width=3)
    if soort == "normaal":
        return c.create_line(alpha*x1,alpha*y1,alpha*x2,alpha*y2,dash=(),fill=kleur,width=1)

    
# create frame to put control buttons onto

frame = Frame(gui)
frame.pack()
Button(frame, text='sinus',command= lambda: start(sin)).pack(side="left")
Button(frame, text='ruis',command= lambda: start(ruis)).pack(side="left")
Button(frame, text='vlak',command= lambda: start(vlak)).pack(side="left")
Button(frame, text='step',command= lambda: start(step)).pack(side="left")
Button(frame, text='trapezium',command= lambda: start(trapezium)).pack(side="left")
scale=Scale(gui, from_=1, to=250)
scale.pack(side="left")
scale.set(150)

#https://mathformeremortals.wordpress.com/2013/01/12/a-numerical-second-derivative-from-three-points/
#https://www.compadre.org/PICUP/resources/Numerical-Integration/