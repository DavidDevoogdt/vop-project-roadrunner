# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 10:50:04 2019

@author: rbnwy
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 02:09:55 2019

@author: rbnwy
"""
from QuarterCar3 import *
from random import *
from collections import *
from operator import itemgetter
import seaborn as sns
import numpy as np 
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 
from scipy.interpolate import interp1d
import pandas as pd
from scipy import signal
from datetime import datetime, timedelta
import csv

plt.close("all")

df = pd.read_csv('kalmandata.csv', delimiter=";", header=0, index_col =0 )
f_az=  interp1d(df['seconds'], df['az0'], kind='cubic')
f_vx= interp1d(df['seconds'][1:], df['v_kalman'][1:], kind='linear')
########### Stuk Keano bumper functie maken ##########
def runningAvg(series, N):
    padded = np.pad(series, (N // 2, N - 1 - N // 2), mode='edge')
    return np.convolve(padded, np.ones((N,)) / N, mode='valid')
df = pd.read_csv('readings/A_4.csv', delimiter=";", header=0, index_col =0 )
cutoff = 20
freq = 100
b, a = signal.butter(2, (cutoff / freq), btype='low', analog=False)
df['az'] = signal.filtfilt(b, a, df['az'])
t2 = df[ df['az'] > 1.6].iloc[0]['seconds']
speed =  df[ df['seconds'] == np.ceil(t2)].iloc[0]['gpsspeed']
x1=[]
y1=[]
with open('justprofiel_bumper.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        y1.append(float(row[3])/1000)
x1 = np.array([0.1*i for i in range(len(y1))])
y1 = np.array(y1)
f_gdrempel=  interp1d(x1, y1, kind='cubic')
x=[]
y=[]
with open('rode bumper profiel.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        try:
            y.append(-float(row[2].replace(',','.')))
        except:
            0

x = np.array([0.1*i for i in range(len(y))])
y = np.array(y)
f_rdrempel = interp1d(x, y, kind='cubic')

def drempel_r(x):
    try: 
        return -float(f_rdrempel(x))
    except:
        return -0.0160697
def drempel_g(x):
    try:
        return -float(f_rdrempel(x))
    except:
        return -0.033
#acceleratie bump begint op 3.65
#plt.plot(np.linspace(1,13.99,600), f_az(np.linspace(1,13.99,600)))
            
def snelheid(t):  #merk op, staat in tijd
    Dt = 3.65
    try:
        return f_vx(t+Dt)
    except:
        return 8.0065
def acceleratie(t):
    Dt = 3.65
    try:
        #let op staat omgekeerd
        return -(f_az(t+Dt)-9.81) #ze hebben die g toegevoegd ma heel formeel is er geen "acceleratie" he alleen valversnelling als kracht
    except:
        return 0
    #einde is op 2.4

#specificaties
signaal=drempel_r #vlak,ruis,step,trapezium,sin
signaal_v=snelheid
algoritme=Euler #RK4,Euler (is 2 à 3 keer sneller maar minder nauwkeurig bij lage fps)
met_shift=True #zonder shift is het achterwiel eerst vlak; vooral interessant bij sin functie zodat de wielen precies hetzelfde doen
allow_flying=False #als de banden niet ingedrukt worden, creëren ze geen kracht
fps=300
tot_tijd=2.4
metExecutietijd=False

h=10**(-6)


def printDict(d):
    out=''
    for i,(key,value) in enumerate(init.items()):
        s=key+': '+str(value)+'\n'
        out+=s
    return out

def randomize(factor):
    for i,(key,value) in enumerate(init.items()):
        init[key]=value*((random()-0.5)*factor+1)
        
#te reconstrueren parameters
doel=OrderedDict()
doel['k_downforce']=0
doel["lengte"]=2.7
doel["afstandautowiel"]=0.89
doel["straal_wiel"]=0.228 
doel["contact_length"]=0.2*0.75
doel["m_a"]=1000
doel["m_w"]=11.5
doel["k_aw"]=60000
doel["k_wg"]=300000
doel["c"]=3000
doel["I"]=3000
doel["l0_autowiel"]=0.89

#mse=-2.218
doel=OrderedDict()
doel['k_downforce']=0
doel["lengte"]=2.7
doel["afstandautowiel"]=0.3
doel["straal_wiel"]=0.228 
doel["contact_length"]=0.2*0
doel["m_a"]=1000
doel["m_w"]=11.5
doel["k_aw"]=62726*2.2
doel["k_wg"]=277412*1.5
doel["c"]=3319*0.8
doel["I"]=3192*0
doel["l0_autowiel"]=0.3


doelSimul=Simulation(*list(doel.values()),algoritme,signaal,signaal_v,met_shift,allow_flying)
doelSimul.simulate(fps,tot_tijd,metExecutietijd)

def jump():
    global init, x_last, grad_x_last
    init=doel.copy()
    randomize(.5)
    #init["m_a"]=501
    for i,(key,value) in enumerate(init.items()):
        init[key]=init[key]*0.9
    x_last=np.array(list(init.values()))
    grad_x_last,m=grad(h)
    for i,(key,value) in enumerate(init.items()):
        init[key]=init[key]*1/0.9
    ####### BELANGRIJK: VASTE PARAMETERS ######    
    init['k_downforce']=0
    init["lengte"]=2.7
    #init["afstandautowiel"]=0.89
    init["straal_wiel"]=0.228 
    init["contact_length"]=0.2*0.75
    init["m_a"]=1000
    init["m_w"]=11.5
    #init["k_aw"]=60000
    #init["k_wg"]=300000
    #init["c"]=3000
    #init["I"]=3000
    #init["l0_autowiel"]=0.89

aantal=int(fps*tot_tijd)
acceleratieArray = np.zeros(aantal)
tijdArray=np.linspace(0,tot_tijd,aantal)
for i in range(aantal):
    acceleratieArray[i] = acceleratie(tijdArray[i])
    
def mse(s1,s2):
    if s1.state == "Regular":
        #op zichzelf:
        #return ((s1.output("hoogteLijst")-s2.output("hoogteLijst"))**2).mean()

        #de echte manier:
        return ((s1.output("accLijst")-acceleratieArray)**2).mean()
    else:
        #print("oops")
        return math.nan
def plot_ifv_h(f):
    data=[]
    for _h in range(-5,5,1):
        data.append(f(10**(-_h))[0])
    plt.figure()        
    for i,v in enumerate(list(np.transpose(np.array(-np.log10(np.abs(data)))))):
        plt.ylabel('-log(mean squared error)')
        plt.title('grad voor elke variable')
        plt.plot(v)
def grad_random_walk(factor,amount):
    data = []
    for i in range(amount):
        randomize(factor)
        data.append(np.array(grad(h)))
    #plt.figure()
    for i in list(np.transpose(np.array(data))):
        plt.plot(i)
    plt.pause(.5)
    plt.show()
#for i in range(2,8):
#    plt.figure(i)
#    grad_random_walk(10**(-i),5)
    
#grad_random_walk(0.000001,10)
def grad(h):
    df=[]
    for i,(key,value) in enumerate(init.items()):
        foo=init[key]
        init[key] = init[key] + h
        s=Simulation(*list(init.values()),algoritme,signaal,signaal_v,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        init[key]=foo
        nextOne = mse(s,doelSimul)
        
        s=Simulation(*list(init.values()),algoritme,signaal,signaal_v,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        currOne = mse(s,doelSimul)
        
        df.append((nextOne-currOne)/h)
    ####### BELANGRIJK: VASTE PARAMETERS ######
    df[0]=0
    df[1]=0
    df[3]=0
    df[4]=0
    df[5]=0
    df[6]=0
    return np.array(df), currOne
def vectorLaplacian(h):
    ddf=[]
    ddf2=[]
    h2=[6,6,4,6,6,4,4,3,2,1,3,3,6]
    for i,(key,value) in enumerate(init.items()):
        foo=init[key]
        init[key] = init[key] + 2*10**(-h2[i])
        s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        init[key]=foo
        nextnextOne = mse(s,doelSimul)
        
        foo=init[key]
        init[key] = init[key] + 10**(-h2[i])
        s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        init[key]=foo
        nextOne = mse(s,doelSimul)
        
        s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        currOne = mse(s,doelSimul)
    
        foo=init[key]
        init[key] = init[key] - 10**(-h2[i])
        s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        init[key]=foo
        prevOne = mse(s,doelSimul)
    
        foo=init[key]
        init[key] = init[key] - 2*10**(-h2[i])
        s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        init[key]=foo
        prevprevOne = mse(s,doelSimul)
        
        ddf.append((-prevprevOne+16*prevOne+16*nextOne-nextnextOne-30*currOne)/(12*h**2))
        ddf2.append((nextOne-2*currOne+prevOne)/(h**2))
    return np.array(ddf)
def hacker_step():
    for i,(key,value) in enumerate(init.items()):
        init[key]=init[key]+(doel[key]-init[key])/2
    s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
    s.simulate(fps,tot_tijd,metExecutietijd)
    return np.array(list(init.values())),np.array(list(init.values())), mse(s,doelSimul)    
def gradientDescent_step():
    x = np.array(list(init.values()))
    grad_x, m = grad(h)
    stepsize = abs(np.dot((x-x_last),grad_x-grad_x_last))/np.dot(grad_x-grad_x_last,grad_x-grad_x_last)
    step = -stepsize*grad_x
    for i,(key,value) in enumerate(init.items()):
        init[key]=init[key]+step[i]
    if m == math.nan or np.isnan(step).any() or np.isnan(x).any():
        print("Simulation failed: jumping again")
        return "fail",grad_x, m
    else:
        return x, grad_x, m
def lineSearch_step():
    x = np.array(list(init.values()))
    grad_x, m = grad(h)
    
    p_last = m
    looseMin = []
    for stepsize in np.linspace(0,1,30):
        step = -stepsize*grad_x                    
        for i,(key,value) in enumerate(init.items()):
            init[key]=init[key]+step[i]
        s=Simulation(*list(init.values()),algoritme,signaal,met_shift,allow_flying)
        s.simulate(fps,tot_tijd,metExecutietijd)
        p=mse(s,doelSimul)
        print(stepsize,p)
        p_deriv = p - p_last
        if p_deriv > 0 or p == math.nan:
            for i,(key,value) in enumerate(init.items()):
                init[key]=init[key]-step[i]
            break
        else:
            looseMin.append((p,step))
        p_last=p
        for i,(key,value) in enumerate(init.items()):
            init[key]=init[key]-step[i]
    step = min(looseMin,key=itemgetter(0))[1]
    print(min(looseMin,key=itemgetter(0)))
    for i,(key,value) in enumerate(init.items()):
        init[key]=init[key]+step[i]
    
    
    
    for i,(key,value) in enumerate(init.items()):
        init[key]=init[key]+step[i]
    if m == math.nan or np.isnan(step).any() or np.isnan(x).any():
        print("Simulation failed: jumping again")
        return "fail",grad_x, m
    else:
        return x, grad_x, m    
def localMin(u):
    withPlotting=True
    maxSteps=400
    minSlope=0.001
    normalizer=list(doel.values())    
    param_data = []
    mse_list = []
    mse_derivative=[]
    if withPlotting:
        fig = plt.figure(u)
        mse_plot = fig.add_subplot(2,1,1)    
        prms = fig.add_subplot(2,1,2)
    
    for k in range(1,maxSteps):
        if k == 1:
            print("Stap %d: " % k)
        else:
            print("Stap %d: log(mse) = %f" % (k,mse_list[-1]))
        x_last, grad_x_last, mse_last = gradientDescent_step()  
        #x_last, grad_x_last, mse_last = lineSearch_step()  
        #x_last, grad_x_last, mse_last = hacker_step()
        if type(x_last) is str:
            plt.close()
            jump()
            localMin(u)
            break
        #gaat steeds geshift zijn met één simulatie maar dat helpt executietijd
        param_data.append(np.divide(x_last,normalizer))
        mse_list.append(math.log(mse_last)) 
        if k > 2:
            mse_derivative.append(mse_list[-1]-mse_list[-2])
        if k > 30 and mse_derivative[-1] > 3:
            print("Wilde jumps-- volgens theorie is dit normaal in de BB methode, voorlopig geven we het op")
            break
        if all(abs(slope)<minSlope for slope in mse_derivative[len(mse_derivative)-5:len(mse_derivative)]) and k > 10:
            if mse_derivative[-1] < 0:
                print("Plateau gedetecteerd (minimum)")
                return (min(mse_list), k, init)
                break
            else:
                print("Plateau gedetecteerd maar op een maximum, we herbeginnen")
                plt.close()
                jump()
                localMin(u)
                break
            
        if withPlotting:
            mse_plot.cla()
            prms.cla()
            mse_plot.plot(mse_list)
            for j,v in enumerate(list(np.transpose(np.array(param_data)))):
                prms.plot(v,label=list(doel.keys())[j])
            prms.legend(loc='upper left')
            figManager = plt.get_current_fig_manager()
            figManager.window.showMaximized()
            plt.pause(.5)
            plt.show()
    return min(mse_list), maxSteps, init
def globalMin(amount):
    minima = []
    for u in range(amount):
        print("------------local search %d/%d------------" % (u+1,amount))
        jump()
        minima.append(localMin(u))
        print(minima)
        print("Lokaal minimum op log(mse) = %f na %d stappen met parameters:" % (minima[-1][0],minima[-1][1]))
    return min(minima,key=itemgetter(0))
        
s=Simulation(*list(doel.values()),algoritme,signaal,signaal_v, met_shift,allow_flying)
s.simulate(fps,tot_tijd,metExecutietijd)
#s.output("accPlot")
#plt.plot(np.linspace(0,2.4,600), acceleratie(np.linspace(0,2.4,600)))
#plt.title(math.log(mse(s,doelSimul)))

globalMin(1)
