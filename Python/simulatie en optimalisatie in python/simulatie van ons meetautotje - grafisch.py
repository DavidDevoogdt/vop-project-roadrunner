from tkinter import *
import time
import math
import numpy as np

class Point():
    def __init__(self,x,y,m,r,color):
        #eigenschappen
        self.m = m       
        self.r = r
        self.color=color
        #dynamica
        self.x = x
        self.y = y
        self.vx=0
        self.vy=0

        self.lastpoint_x=self.x
        self.lastpoint_y=self.y
        self.trace=[]
    def update(self):
        #trace
        stukje = line(self.lastpoint_x,self.lastpoint_y,self.x,self.y,"stippelijn","red")
        self.trace.append((stukje,t))
        self.lastpoint_x=self.x
        self.lastpoint_y=self.y
class SolidPoint():
    def __init__(self,x0,y0,fcn_x,fcn_y):
        self.r=0
        self.x=x0
        self.y=y0
        self.x0=x0
        self.y0=y0
        self.fcn_x=fcn_x
        self.fcn_y=fcn_y
        self.lastpoint_x=self.x0
        self.lastpoint_y=self.y0
        
        self.trace = []
    def update(self):
        self.x=self.x0+self.fcn_x(vx*t)
        self.y=self.y0+self.fcn_y(vx*t)
        
        
        #trace
        stukje = line(self.lastpoint_x,self.lastpoint_y,self.x,self.y,"stippelijn","black")
        self.trace.append((stukje,t))
        self.lastpoint_x=self.x
        self.lastpoint_y=self.y
    def getHeigth(self,tijd):
        return self.y0+self.fcn_y(vx*tijd)    
class QuarterTyre():
    def __init__(self, x0, wegdek, shift):
        #shift in seconden
        self.wegdek=lambda x: wegdek(x-shift) if x > shift else 0

        self.x0=x0
        self.afstandautowiel=0.02
        self.wielgrond=0.01
        self.m_a=1
        self.m_w=0.5
        self.k_aw=100000
        self.k_wg=100000
        self.c=200
        self.I=0

        self.auto = Point(self.x0,canvas_heigth/2-self.afstandautowiel-self.wielgrond,self.m_a,0.01,"black")
        self.wiel = Point(self.x0,canvas_heigth/2-self.wielgrond,self.m_w,0.01,"black")
        self.grond = SolidPoint(self.x0,canvas_heigth/2,lambda x: x,self.wegdek) 
        
        self.l0_1=self.wiel.y-self.auto.y
        self.l0_2=self.grond.y-self.wiel.y
        self.auto.vx=vx
        self.wiel.vx=vx
        
        self.shapes = []
    def update(self):
        w=np.array([self.auto.y,self.auto.vy,self.wiel.y,self.wiel.vy])
        z=RK4(self.f,t,w)
        self.auto.y  = z[0]
        self.auto.vy = z[1]
        self.wiel.y  = z[2]
        self.wiel.vy = z[3]
        #print(self.auto.y)
        
        self.auto.x+=vx*dt
        self.wiel.x+=vx*dt
        self.grond.update()
        if auto_trace==True:
            self.auto.update()
        #eerst alle vormen wegdoen (niet de trace) en dan weer terug tekenen
        for v in self.shapes:
            c.delete(v)
            
        self.shapes=[
                oval(self.auto.x-self.auto.r, self.auto.y-self.auto.r, self.auto.x+self.auto.r, self.auto.y+self.auto.r,self.auto.color),
                oval(self.wiel.x-self.wiel.r, self.wiel.y-self.wiel.r, self.wiel.x+self.wiel.r, self.wiel.y+self.wiel.r,self.wiel.color),
                oval(self.wiel.x-self.wiel.r/2, self.wiel.y-self.wiel.r/2, self.wiel.x+self.wiel.r/2, self.wiel.y+self.wiel.r/2,"grey"),
                #oval(self.grond.x-self.grond.r, self.grond.y-self.grond.r, self.grond.x+self.grond.r, self.grond.y+self.grond.r,"black"),                                
                
                rectangle(self.auto.x,self.auto.y,self.wiel.x,self.wiel.y,"blue"),
                line(self.wiel.x,self.wiel.y,self.auto.x,self.auto.y,"vol","green"),
                line(self.wiel.x,self.wiel.y,self.grond.x,self.grond.y,"vol","red")
                ]    

    def a(self,tijd):
        spring_aw = self.k_aw*(abs(self.auto.y-self.wiel.y)-self.l0_1)
        spring_wg = self.k_wg*(abs(self.wiel.y-self.grond.getHeigth(tijd))-self.l0_2)
        damper_aw = self.c*(self.auto.vy-self.wiel.vy)
        return (spring_aw - damper_aw)/self.m_a
    def f(self, tijd, w):
        #diff vgl dw/dt=f(t,w)

        #w=[y_a,vy_a,y_w,vy_w]
        #z=[v_a,ay_a,v_w,ay_w]
        
        spring_aw = self.k_aw*(abs(w[0]-w[2])-self.l0_1)
        spring_wg = self.k_wg*(abs(w[2]-self.grond.getHeigth(tijd))-self.l0_2)
        damper_aw = self.c*(w[1]-w[3])
        
        my_accel = (spring_aw - damper_aw)/self.m_a
              
        #procentuele fout door 1ste orde benadering
        #print((math.atan((self.otherWheel.auto.y-w[0])/b)-(((self.otherWheel.auto.y-w[0])/b)))/(math.atan((self.otherWheel.auto.y-w[0])/b)*2)*100)
        
        h2=self.otherWheel.auto.y
        h1=w[0]
        b = abs(self.otherWheel.x0-self.x0)
        cosatan=math.cos(math.atan((h2-h1)/b))
        ding=h1**2-2*h1*h2+h2**2+b**2
        koppel=-self.I/ding**2*(b*ding*(my_accel-self.otherWheel.a(tijd))-2*(w[1]-self.otherWheel.auto.vy)**2*(h1-h2))*cosatan        
        
        z=np.zeros(4)
        z[0]=w[1]
        z[2]=w[3]
        
        z[1] = (spring_aw - damper_aw - koppel)/self.m_a - g
        z[3] = (-spring_aw + spring_wg + damper_aw)/self.m_w -g

        return z

def RK4(f, tijd, w):
    #return w + f(tijd, w)*dt        
    s1 = f(tijd, w)
    s2 = f(tijd + dt/2.0, w + dt*s1/2.0)
    s3 = f(tijd + dt/2.0, w + dt*s2/2.0)
    s4 = f(tijd + dt, w + dt*s3)
    
    return w + dt/6.0*(s1 + 2.0*s2 + 2.0*s3 + s4)
def Euler(f, tijd, w):
    return w + f(tijd,w)*dt

#signalen
sin  = lambda x: A*math.sin(x*2*math.pi*f)
ruis = lambda x: A*math.sin(x*1.1*2*math.pi)*math.cos(x*1.7*2*math.pi)*math.sin(x*2.7*2*math.pi)
vlak = lambda x: 0
def step(x):
    if x >= 0.5:
        return -0.1
    else:
        return 0
def trapezium(x):
    A=0.13
    xh=1
    xr=3
    if x<xh:
        return -x*A/xh #y-richting staat omgekeerd...
    elif x>=xh and x<=xh+xr:
        return -A
    elif x>xh+xr and x<2*xh+xr:
        return (x-xh-xr)*A/xh-A
    else:
        return 0

#start simulatie
button_pressed=False
def start(signaal):    
    global c,t,tijd_text,fps,dt,g,vx,breedte,alpha,A,f,auto_trace
    try: 
        c.destroy()
    except:
        foo=1
    c = Canvas(gui ,width=alpha*canvas_width,height=alpha*canvas_heigth)
    c.pack()
    
    button_pressed=True

    t=0
    tijd_text=None
    fps=300
    dt=1/fps
    alpha=scale.get()
    g=0 #staat omgekeerd

    #wagen bouwen
    vx=5 #m/s
    breedte = 0.25
    A=0.2
    f=1/breedte*2 #m^(-1), in plaats
    if signaal == sin:
        print("%d Hz gevoeld door een wiel" % (vx*f)) #Hz, in tijd, gevoeld door een wiel
    met_shift=True
    auto_trace=True
    achter=QuarterTyre(0.5,signaal,breedte*met_shift)
    voor=QuarterTyre(0.5+breedte,signaal,0)
    achter.otherWheel=voor
    voor.otherWheel=achter
    wagen = [achter,voor]
    verbinding = line(wagen[0].auto.x,wagen[0].auto.y,wagen[1].auto.x,wagen[1].auto.y,"vol","black")
    #assenstelsel
    c.create_line(0.5*alpha, (canvas_heigth/2+1.5)*alpha, 1.5*alpha,(canvas_heigth/2+1.5)*alpha-0.5,dash=(),fill="black",arrow=LAST)
    c.create_line(0.5*alpha, (canvas_heigth/2+1.5)*alpha, 0.5*alpha,(canvas_heigth/2+0.5)*alpha-0.5,dash=(),fill="black",arrow=LAST)

    while True:
        t+=dt
        c.delete(tijd_text)
        tijd_text = c.create_text(2*alpha,1*alpha,text="t={0:.{1}f} s".format(t,4)) #user seconde =/= simulatie seconde
        
        for k in wagen:
            k.update()
            c.delete(verbinding)
            verbinding = line(wagen[0].auto.x,wagen[0].auto.y,wagen[1].auto.x,wagen[1].auto.y,"vol","black")
            gui.update()
            time.sleep(dt) #in seconden als het niet lagt
            
#initializeren   
alpha = 200 #1 meter in SI is zoveel pixels
canvas_width=16
canvas_heigth=8
gui = Tk()
gui.geometry("{}x{}".format(canvas_width*alpha,canvas_heigth*alpha))
gui.title("VOP Runge-Kutta 4de orde")


def oval(x1,y1,x2,y2,kleur):
    return c.create_oval(alpha*x1,alpha*y1,alpha*x2,alpha*y2,fill=kleur)
def rectangle(x1,y1,x2,y2,kleur):
    return c.create_rectangle(alpha*x1,alpha*y1,alpha*x2,alpha*y2,outline=kleur,width=3)
def line(x1,y1,x2,y2,soort,kleur):
    if soort == "stippelijn":    
        return c.create_line(alpha*x1,alpha*y1,alpha*x2,alpha*y2,dash=(2, 4),fill=kleur,width=1)
    if soort == "vol":
        return c.create_line(alpha*x1,alpha*y1,alpha*x2,alpha*y2,dash=(),fill=kleur,width=3)

    
# create frame to put control buttons onto
frame = Frame(gui)
frame.pack()
Button(frame, text='sinus',command= lambda: start(sin)).pack(side="left")
Button(frame, text='ruis',command= lambda: start(ruis)).pack(side="left")
Button(frame, text='vlak',command= lambda: start(vlak)).pack(side="left")
Button(frame, text='step',command= lambda: start(step)).pack(side="left")
Button(frame, text='trapezium',command= lambda: start(trapezium)).pack(side="left")
scale=Scale(gui, from_=alpha/2, to=alpha*2)
scale.pack(side="left")
scale.set(alpha)

#scherm kruisje duwen stopt programma
while gui.state() == 'normal':
    gui.mainloop()

#https://mathformeremortals.wordpress.com/2013/01/12/a-numerical-second-derivative-from-three-points/
#https://www.compadre.org/PICUP/resources/Numerical-Integration/