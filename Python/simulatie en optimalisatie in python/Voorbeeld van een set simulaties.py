# -*- coding: utf-8 -*-
"""
Created on Fri Mar 22 02:09:55 2019

@author: rbnwy
"""
from QuarterCar import *
from random import *
from collections import *
plt.clf()

#basic param
# =============================================================================
# vx=5
# k_downforce=0*9.81/vx**2
# lengte=2.5
# afstandautowiel=0.8
# straal_wiel=0.2 
# contact_length=straal_wiel/2.5
# m_a=500
# m_w=50
# k_aw=60000
# k_wg=300000
# c=3000
# I=3000
# l0_autowiel=afstandautowiel
# param=[vx,k_downforce,lengte,afstandautowiel,straal_wiel,contact_length,m_a,m_w,k_aw,k_wg,c,I,l0_autowiel]
# =============================================================================

param=OrderedDict()
param['vx']=5
param['k_downforce']=0.3*9.81/5**2 #0.3g (?)
param["lengte"]=2.5
param["afstandautowiel"]=0.8
param["straal_wiel"]=0.2 
param["contact_length"]=0.2*40/100 #20% vd diamter
param["m_a"]=500
param["m_w"]=50
param["k_aw"]=60000
param["k_wg"]=300000
param["c"]=3000
param["I"]=3000
param["l0_autowiel"]=0.8

#verdere specificaties
signaal=trapezium #vlak,ruis,step,trapezium,sin
algoritme=RK4 #RK4,Euler (is 2 à 3 keer sneller maar minder nauwkeurig bij lage fps)
met_shift=False #zonder shift is het achterwiel eerst vlak; vooral interessant bij sin functie zodat de wielen precies hetzelfde doen
allow_flying=False #als de banden niet ingedrukt worden, creëren ze geen kracht
fps=300
tot_tijd=2
metExecutietijd=True

#elke parameter beetje veranderen en plotten, als de simulatie niet gefaald is
for i,(key,value) in enumerate(param.items()):
    print("--------Simulatie %d--------" % (i+1))
    param[key]=value*(random()+0.5) #elke waarde van 50% tot 150% eens veranderen, afronden en zien wat er gebeurt
    print("%s=%f" % (key,param[key]))
    
    simul=Simulation(*list(param.values()),algoritme,signaal,met_shift,allow_flying)
    simul.simulate(fps,tot_tijd,metExecutietijd)
    if simul.state=="Regular":
        simul.output("hoogtePlot")
        if i==9:
            simul.output("Gui") #keuze tussen: hoogtePlot, accPlot, hoogteLijst, accLijst, Gui
    
    param[key]=value #reset de parameter weer
    
#welke kleur is welke simulatie? https://www.google.com/search?q=python+matplotlib+colors+plot+default+order&client=firefox-b-d&tbm=isch&source=iu&ictx=1&fir=VlEUgfLo7qVfnM%253A%252CPOOIuJ14OgUSJM%252C_&vet=1&usg=AI4_-kQtoeK_Dg621iVi0Kb7JM2Fp1_K6g&sa=X&ved=2ahUKEwi18sTGqaXhAhVNmbQKHZ2zA6QQ9QEwAHoECAsQBg#imgrc=VlEUgfLo7qVfnM: