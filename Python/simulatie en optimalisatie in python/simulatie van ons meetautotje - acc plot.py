from tkinter import *
import time
import math
import numpy as np
import matplotlib.pyplot as plt

class Point():
    def __init__(self,x,y,m,r,color):
        #eigenschappen
        self.m = m       
        self.r = r
        self.color=color
        #dynamica
        self.x = x
        self.y = y
        self.vx=0
        self.vy=0
class SolidPoint():
    def __init__(self,x0,y0,fcn_x,fcn_y):
        self.r=0.02
        self.x=x0
        self.y=y0
        self.x0=x0
        self.y0=y0
        self.fcn_x=fcn_x
        self.fcn_y=fcn_y
        self.lastpoint_x=self.x0
        self.lastpoint_y=self.y0
        
    def update(self):
        self.x=self.x0+self.fcn_x(vx*t)
        self.y=self.y0+self.fcn_y(vx*t)
        
    def getHeigth(self,tijd):
        return self.y0+self.fcn_y(vx*tijd)    
class QuarterTyre():
    def __init__(self, x0, wegdek, shift):
        #shift in seconden
        self.wegdek=lambda x: wegdek(x-shift) if x > shift else 0

        self.x0=x0
        self.afstandautowiel=0.02
        self.wielgrond=0.01
        self.m_a=1
        self.m_w=0.5
        self.k_aw=10000000
        self.k_wg=10000000
        self.c=700
        self.I=0

        
        self.auto = Point(self.x0,-self.afstandautowiel-self.wielgrond,self.m_a,0.01,"black")
        self.wiel = Point(self.x0,-self.wielgrond,self.m_w,0.2,"black")
        self.grond = SolidPoint(self.x0,0,lambda x: x,self.wegdek) 
        
        self.l0_1=self.wiel.y-self.auto.y
        self.l0_2=self.grond.y-self.wiel.y
        self.auto.vx=vx
        self.wiel.vx=vx
    def update(self):
        w=np.array([self.auto.y,self.auto.vy,self.wiel.y,self.wiel.vy])
        z=RK4(self.f,t,w)
        self.auto.y  = z[0]
        self.auto.vy = z[1]
        self.wiel.y  = z[2]
        self.wiel.vy = z[3]
        hoogte[frame]=z[0]
        acc[frame]=self.f(t,w)[1]
        
        self.auto.x+=vx*dt
        self.wiel.x+=vx*dt
        self.grond.update()

    def a(self,tijd):
        spring_aw = self.k_aw*(abs(self.auto.y-self.wiel.y)-self.l0_1)
        spring_wg = self.k_wg*(abs(self.wiel.y-self.grond.getHeigth(tijd))-self.l0_2)
        damper_aw = self.c*(self.auto.vy-self.wiel.vy)
        return (spring_aw - damper_aw)/self.m_a
    def f(self, tijd, w):
        #diff vgl dw/dt=f(t,w)

        #w=[y_a,vy_a,y_w,vy_w]
        #z=[v_a,ay_a,v_w,ay_w]
        
        spring_aw = self.k_aw*(abs(w[0]-w[2])-self.l0_1)
        spring_wg = self.k_wg*(abs(w[2]-self.grond.getHeigth(tijd))-self.l0_2)
        damper_aw = self.c*(w[1]-w[3])
        
        my_accel = (spring_aw - damper_aw)/self.m_a
              
        #procentuele fout door 1ste orde benadering
        #print((math.atan((self.otherWheel.auto.y-w[0])/b)-(((self.otherWheel.auto.y-w[0])/b)))/(math.atan((self.otherWheel.auto.y-w[0])/b)*2)*100)
        
        h2=self.otherWheel.auto.y
        h1=w[0]
        b = abs(self.otherWheel.x0-self.x0)
        cosatan=math.cos(math.atan((h2-h1)/b))
        ding=h1**2-2*h1*h2+h2**2+b**2
        koppel=-self.I/ding**2*(b*ding*(my_accel-self.otherWheel.a(tijd))-2*(w[1]-self.otherWheel.auto.vy)**2*(h1-h2))*cosatan        
        
        z=np.zeros(4)
        z[0]=w[1]
        z[2]=w[3]
        
        z[1] = (spring_aw - damper_aw - koppel)/self.m_a - g
        z[3] = (-spring_aw + spring_wg + damper_aw)/self.m_w -g

        return z

def RK4(f, tijd, w):
    #return w + f(tijd, w)*dt        
    s1 = f(tijd, w)
    s2 = f(tijd + dt/2.0, w + dt*s1/2.0)
    s3 = f(tijd + dt/2.0, w + dt*s2/2.0)
    s4 = f(tijd + dt, w + dt*s3)
    
    return w + dt/6.0*(s1 + 2.0*s2 + 2.0*s3 + s4)
def Euler(f, tijd, w):
    return w + f(tijd,w)*dt

#signalen
sin  = lambda x: 0.2*math.sin(x*2*math.pi)
ruis = lambda x: 0.1*math.sin(x*1.1*2*math.pi)*math.cos(x*1.7*2*math.pi)*math.sin(x*2.7*2*math.pi)
vlak = lambda x: 0
#step = lambda t: -0.1 if t>0.5*dt else 0
def step(x):
    if x >= 0.5:
        return -0.1
    else:
        return 0
def trapezium(x):
    A=0.13
    xh=1
    xr=3
    if x<xh:
        return -x*A/xh #y-richting staat omgekeerd...
    elif x>=xh and x<=xh+xr:
        return -A
    elif x>xh+xr and x<2*xh+xr:
        return (x-xh-xr)*A/xh-A
    else:
        return 0

g=0 #later optellen!
vx=5
breedte = 0.25
met_shift=True
signaal=trapezium
achter = QuarterTyre(0,       signaal, breedte*met_shift)
voor   = QuarterTyre(breedte, signaal, 0)
achter.otherWheel=voor
voor.otherWheel=achter
wagen = [achter,voor]

#animeren
t=0
fps=10000
tot_tijd=2
dt=1/fps
start_time=time.time()
tijd=np.linspace(0,tot_tijd,fps*tot_tijd)
hoogte=np.zeros(fps*tot_tijd)
acc=np.zeros(fps*tot_tijd)
for i in range(fps*2):
    t=i*dt
    frame=i
    for k in wagen:
        k.update()
print("Executietijd: %s ms" % (1000*(time.time()-start_time)))

plt.plot(tijd,-hoogte)        
plt.plot(tijd,-acc)        

#https://mathformeremortals.wordpress.com/2013/01/12/a-numerical-second-derivative-from-three-points/
#https://www.compadre.org/PICUP/resources/Numerical-Integration/