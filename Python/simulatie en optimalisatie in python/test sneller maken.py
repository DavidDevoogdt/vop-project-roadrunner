# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 21:27:01 2019

@author: rbnwy
"""
from QuarterCarSnel import *
from random import *
from collections import *


signaal=trapezium #vlak,ruis,step,trapezium,sin
algoritme='' #RK4,Euler (is 2 à 3 keer sneller maar minder nauwkeurig bij lage fps)
met_shift=False #zonder shift is het achterwiel eerst vlak; vooral interessant bij sin functie zodat de wielen precies hetzelfde doen
allow_flying=False #als de banden niet ingedrukt worden, creëren ze geen kracht
fps=300
tot_tijd=2
metExecutietijd=True

h=10**(-6)


def printDict(d):
    out=''
    for i,(key,value) in enumerate(init.items()):
        s=key+': '+str(value)+'\n'
        out+=s
    return out

def randomize(factor):
    for i,(key,value) in enumerate(init.items()):
        init[key]=value*((random()-0.5)*factor+1)
        
#te reconstrueren parameters
doel=OrderedDict()
doel['vx']=5
doel['k_downforce']=0.3*9.81/5**2 #0.3g (?)
doel["lengte"]=2.5
doel["afstandautowiel"]=0.8
doel["straal_wiel"]=0.2 
doel["contact_length"]=0.2*40/100 #20% vd diamter
doel["m_a"]=500
doel["m_w"]=50
doel["k_aw"]=60000
doel["k_wg"]=300000
doel["c"]=3000
doel["I"]=3000
doel["l0_autowiel"]=0.8

doelSimul=Simulation(*list(doel.values()),algoritme,signaal,met_shift,allow_flying)
doelSimul.simulate(fps,tot_tijd,metExecutietijd)
doelSimul.output("hoogtePlot")
