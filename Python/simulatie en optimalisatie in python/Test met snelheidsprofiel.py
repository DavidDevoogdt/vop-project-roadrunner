# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 14:42:53 2019

@author: rbnwy
"""

from QuarterCar3 import *
from random import *
from collections import *
from operator import itemgetter
import seaborn as sns
import numpy as np 
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D 
from scipy.interpolate import interp1d
import pandas as pd
from scipy import signal
from datetime import datetime, timedelta
import csv

plt.close("all")

df = pd.read_csv('kalmandata.csv', delimiter=";", header=0, index_col =0 )
f_az=  interp1d(df['seconds'], df['az0'], kind='cubic')
f_vx= interp1d(df['seconds'][1:], df['v_kalman'][1:], kind='cubic')
########### Stuk Keano bumper functie maken ##########
def runningAvg(series, N):
    padded = np.pad(series, (N // 2, N - 1 - N // 2), mode='edge')
    return np.convolve(padded, np.ones((N,)) / N, mode='valid')
df = pd.read_csv('readings/A_4.csv', delimiter=";", header=0, index_col =0 )
cutoff = 20
freq = 100
b, a = signal.butter(2, (cutoff / freq), btype='low', analog=False)
df['az'] = signal.filtfilt(b, a, df['az'])
t2 = df[ df['az'] > 1.6].iloc[0]['seconds']
speed =  df[ df['seconds'] == np.ceil(t2)].iloc[0]['gpsspeed']
x1=[]
y1=[]
with open('justprofiel_bumper.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        y1.append(float(row[3])/1000)
x1 = np.array([0.1*i for i in range(len(y1))])
y1 = np.array(y1)
f_gdrempel=  interp1d(x1, y1, kind='cubic')
x=[]
y=[]
with open('rode bumper profiel.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=';')
    for row in readCSV:
        try:
            y.append(-float(row[2].replace(',','.')))
        except:
            0

x = np.array([0.1*i for i in range(len(y))])
y = np.array(y)
f_rdrempel = interp1d(x, y, kind='cubic')

def drempel_r(x):
    try: 
        return -float(f_rdrempel(x))
    except:
        return -0.0160697
def drempel_g(x):
    try:
        return -float(f_gdrempel(x))
    except:
        return -0.033

#acceleratie bump begint op 3.65
#plt.plot(np.linspace(1,13.99,600), f_az(np.linspace(1,13.99,600)))
        
def snelheid(t):  #merk op, staat in tijd
    Dt = 3.65
    try:
        return float(f_vx(t+Dt))
    except:
        return 8.0065
def acceleratie(t):
    Dt = 3.65
    try:
        return f_az(t+Dt)-9.81 #ze hebben die toegevoegd ma heel formeel is er geen "acceleratie" he alleen valversnelling als kracht
    except:
        return 0
    #einde is op 2.4
    
#specificaties
signaal=drempel_g #vlak,ruis,step,trapezium,sin
#signaal_v=lambda x: -step(x/2)*100*x
signaal_v=snelheid
algoritme=Euler #RK4,Euler (is 2 à 3 keer sneller maar minder nauwkeurig bij lage fps)
met_shift=True #zonder shift is het achterwiel eerst vlak; vooral interessant bij sin functie zodat de wielen precies hetzelfde doen
allow_flying=True #als de banden niet ingedrukt worden, creëren ze geen kracht
fps=300
tot_tijd=2.4     #einde is op 2.4
metExecutietijd=False

h=10**(-6)


def printDict(d):
    out=''
    for i,(key,value) in enumerate(init.items()):
        s=key+': '+str(value)+'\n'
        out+=s
    return out

def randomize(factor):
    for i,(key,value) in enumerate(init.items()):
        init[key]=value*((random()-0.5)*factor+1)
        
#te reconstrueren parameters
doel=OrderedDict()
doel['k_downforce']=0.3*9.81/5**2 #0.3g (?)
doel["lengte"]=2.5
doel["afstandautowiel"]=0.8
doel["straal_wiel"]=0.2 
doel["contact_length"]=0.2*40/100 #20% vd diamter
doel["m_a"]=500
doel["m_w"]=50
doel["k_aw"]=60000
doel["k_wg"]=300000
doel["c"]=3000
doel["I"]=3000
doel["l0_autowiel"]=0.8

s=Simulation(*list(doel.values()),algoritme,signaal,signaal_v,met_shift,allow_flying)
s.simulate(fps,tot_tijd,metExecutietijd)
s.output("accPlot")
plt.plot(np.linspace(0,2.4,1000), acceleratie(np.linspace(0,2.4,1000)))